﻿using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.IO;

namespace Lesson_10
{
    class Program
    {
        static void Main(string[] args)
        {
            const string CsvFile = "reflections.csv";
            const int Attempts = 10000;

            TakeMeasurementsByMyself_Streams(CsvFile, Attempts);
            TakeMeasurementsByMyself_string(Attempts);
            TakeMeasurementsByNewtonsoft(CsvFile, Attempts);
        }

        private static void TakeMeasurementsByMyself_Streams(string file, int numberOfAttempts)
        {
            Console.WriteLine($"Now will execute {System.Reflection.MethodBase.GetCurrentMethod().Name}. Number of attempts: {numberOfAttempts}");
            Console.WriteLine(Environment.NewLine);

            var data = MyClass.CreateRandomInstance();
            var deserialized = default(MyClass);
            var serializationStopWatch = new Stopwatch();
            var deserializationStopWatch = new Stopwatch();

            for (var i = 1; i <= numberOfAttempts; i++)
            {
                serializationStopWatch.Start();
                using (var stream = new FileStream(file, FileMode.Create, FileAccess.Write))
                {
                    var cs = new CsvSerializer<MyClass>();
                    cs.Serialize(stream, data);
                }
                serializationStopWatch.Stop();

                deserializationStopWatch.Start();
                using (var stream = new FileStream(file, FileMode.Open, FileAccess.Read))
                {
                    var cs = new CsvSerializer<MyClass>();
                    deserialized = cs.Deserialize(stream);
                }
                deserializationStopWatch.Stop();

                if (!data.Equals(deserialized))
                {
                    Console.WriteLine($"Objects equality is: {data.Equals(deserialized)}");
                }
            }

            Console.WriteLine("Cicles is over.");
            Console.WriteLine($"Serialization time -\t{serializationStopWatch.ElapsedMilliseconds} ms");
            Console.WriteLine($"Deserialization time -\t{deserializationStopWatch.ElapsedMilliseconds} ms");
            Console.WriteLine("Execution is over.");
            Console.WriteLine(Environment.NewLine);
        }

        private static void TakeMeasurementsByMyself_string(int numberOfAttempts)
        {
            Console.WriteLine($"Now will execute {System.Reflection.MethodBase.GetCurrentMethod().Name}. Number of attempts: {numberOfAttempts}");
            Console.WriteLine(Environment.NewLine);

            var cs = new CsvSerializer<MyClass>();
            var data = MyClass.CreateRandomInstance();
            var serialized = default(string);
            var deserialized = default(MyClass);
            var serializationStopWatch = new Stopwatch();
            var deserializationStopWatch = new Stopwatch();

            for (var i = 1; i <= numberOfAttempts; i++)
            {
                serializationStopWatch.Start();
                serialized = cs.SerializeToString(data);
                serializationStopWatch.Stop();

                deserializationStopWatch.Start();
                deserialized = cs.DeserializeFromString(serialized);
                deserializationStopWatch.Stop();

                if (!data.Equals(deserialized))
                {
                    Console.WriteLine($"Objects equality is: {data.Equals(deserialized)}");
                }
            }

            Console.WriteLine("Cicles is over.");
            Console.WriteLine($"Serialization time -\t{serializationStopWatch.ElapsedMilliseconds} ms");
            Console.WriteLine($"Deserialization time -\t{deserializationStopWatch.ElapsedMilliseconds} ms");
            Console.WriteLine("Execution is over.");
            Console.WriteLine(Environment.NewLine);
        }

        private static void TakeMeasurementsByNewtonsoft(string file, int numberOfAttempts)
        {
            Console.WriteLine($"Now will execute {System.Reflection.MethodBase.GetCurrentMethod().Name}. Number of attempts: {numberOfAttempts}");
            Console.WriteLine(Environment.NewLine);

            var data = MyClass.CreateRandomInstance();
            var deserialized = default(MyClass);
            var serializationStopWatch = new Stopwatch();
            var deserializationStopWatch = new Stopwatch();

            for (var i = 1; i <= numberOfAttempts; i++)
            {
                serializationStopWatch.Start();
                var result = JsonConvert.SerializeObject(data);
                serializationStopWatch.Stop();

                deserializationStopWatch.Start();
                deserialized = (MyClass)JsonConvert.DeserializeObject(result, typeof(MyClass));
                deserializationStopWatch.Stop();

                if (!data.Equals(deserialized))
                {
                    Console.WriteLine($"Objects equality is: {data.Equals(deserialized)}");
                }
            }

            Console.WriteLine("Cicles is over.");
            Console.WriteLine($"Serialization time -\t{serializationStopWatch.ElapsedMilliseconds} ms");
            Console.WriteLine($"Deserialization time -\t{deserializationStopWatch.ElapsedMilliseconds} ms");
            Console.WriteLine("Execution is over.");
            Console.WriteLine(Environment.NewLine);
        }
    }
}
