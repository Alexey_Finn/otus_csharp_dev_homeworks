﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Lesson_10
{
    public class CsvSerializer<T> where T : class, new()
    {
        const string FieldsSeparator = ";";
        const string PrivateSeparator = ",";

        private List<FieldInfo> _fields;
        public CsvSerializer()
        {
            var type = typeof(T);
            _fields = type.GetFields().OrderBy(f => f.Name).ToList();
        }

        public string SerializeToString(T data)
        {
            var sb = new StringBuilder();
            var values = new List<string>();

            //sb.AppendLine(data.GetType().Name.ToString());

            foreach (var f in _fields)
            {
                var field = f.GetValue(data);
                var type = f.FieldType.Name;
                values.Add(string.Join(PrivateSeparator, f.Name, type, field == null ? "" : field.ToString()));
            }

            sb.AppendLine(string.Join(FieldsSeparator, values));

            return sb.ToString();
        }

        public void Serialize(Stream stream, T data)
        {
            var sb = new StringBuilder();
            var values = new List<string>();

            //sb.AppendLine(data.GetType().Name.ToString());

            foreach (var f in _fields)
            {
                var field = f.GetValue(data);
                var type = f.FieldType.Name;
                values.Add(string.Join(PrivateSeparator, f.Name, type, field == null ? "" : field.ToString()));
            }

            sb.AppendLine(string.Join(FieldsSeparator, values));

            using (var sw = new StreamWriter(stream))
            {
                sw.Write(sb.ToString().Trim());
            }
        }

        public T DeserializeFromString(string content)
        {
            var result = (T)Activator.CreateInstance(typeof(T));

            var fieldscontent = content.Split(FieldsSeparator);

            foreach (var part in fieldscontent)
            {
                var members = part.Split(PrivateSeparator);

                var fieldName = _fields.Single(f => f.Name == members[0]).Name;
                var typeName = members[1];

                // ! не понял как протащить тип данных и создать член класса конкретного типа
                //var type = Activator.CreateInstance(null, typeName);
                var fieldValue = int.Parse(members[2]);

                var field = typeof(T).GetField(fieldName);
                field.SetValue(result, fieldValue);
            }

            return result;
        }

        public T Deserialize(Stream stream)
        {
            var content = default(string);
            var result = (T)Activator.CreateInstance(typeof(T));

            using (var sr = new StreamReader(stream))
            {
                content = sr.ReadToEnd();
            }

            var fieldscontent = content.Split(FieldsSeparator);

            foreach (var part in fieldscontent)
            {
                var members = part.Split(PrivateSeparator);

                var fieldName = _fields.Single(f => f.Name == members[0]).Name;
                var typeName = members[1];

                // ! не понял как протащить тип данных и создать член класса конкретного типа
                //var type = Activator.CreateInstance(null, typeName);
                var fieldValue = int.Parse(members[2]);

                var field = typeof(T).GetField(fieldName);
                field.SetValue(result, fieldValue);
            }

            return result;
        }        
    }
}