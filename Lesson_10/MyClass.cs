﻿using System;

namespace Lesson_10
{
    [Serializable]
    public class MyClass : IEquatable<MyClass>
    {
        public int field1, field2, field3, field4, field5, field11, field12, field13, field14;

        public static MyClass CreateRandomInstance() => new() 
        { 
            field1 = GetRandom(), field2 = GetRandom(), 
            field3 = GetRandom(), field4 = GetRandom(), 
            field5 = GetRandom(), 
            field11 = GetRandom(), field12 = GetRandom(), 
            field13 = GetRandom(), field14 = GetRandom(), 
        };

        private static int GetRandom()
        {
            var rnd = new Random();
            return rnd.Next(1, 100);
        }

        bool IEquatable<MyClass>.Equals(MyClass other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;
            if (other.GetType() != GetType()) return false;
            return Equals(other);
        }
        private bool Equals(MyClass other)
        {
            return field1 == other.field1 &&
                field2 == other.field2 &&
                field3 == other.field3 && 
                field4 == other.field4 &&
                field5 == other.field5 &&
                field11 == other.field11 && 
                field12 == other.field12 &&
                field13 == other.field13 &&
                field14 == other.field14;
        }

        public override bool Equals(object obj)
        {
            return ((IEquatable<MyClass>)this).Equals(obj as MyClass);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(field12, field13, field3, field4, field5, field11, field12, field14);
        }
    }
}