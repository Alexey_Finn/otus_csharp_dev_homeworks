﻿using System;

namespace Lesson_13
{
	internal class NullToDoTask : ToDoTask
	{
		internal NullToDoTask() : base("Null task", DateTime.Now)
		{
		}
		public override bool IsNull() => true;

		public override bool Equals(object obj) => base.Equals(obj);

		public override int GetHashCode() => base.GetHashCode();

		public override string ToString() => $"Null ToDoTask";		
	}
}
