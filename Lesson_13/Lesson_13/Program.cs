﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace Lesson_13
{
	class Program
	{
		const string _fileName = "todo.txt";
		const string _logFileName = "log.txt";

		static void AddTask(string name, DateTime date)
		{
			ToDoTask task;

			try
			{
				task = new ToDoTask(name, date);
			}
			catch (ArgumentException e)
			{
				Console.WriteLine($"Error: {e.Message}.");
				task = new NullToDoTask();
			}

			if (task.IsNull())
			{
				Console.WriteLine($"Cannot add task with specified parameters. Try to reenter parameters.");
				return;
			}

			if (!File.Exists(_fileName))
			{
				Console.Write($"File with tasks not exists. Create new? (Y/n)\t");
				var inputKey = Console.ReadKey(true);
				Console.WriteLine(Environment.NewLine);

				if (inputKey == new ConsoleKeyInfo('n', ConsoleKey.N, shift: false, alt: false, control: false))
				{
					Console.WriteLine("New file with tasks will not be created. Task will not be saved");
					return;
				}
				else
				{
					Console.WriteLine($"New file with tasks created.");
					File.Create(_fileName);
				}
			}

			File.AppendAllLines(_fileName, new[] { $"{task.Date:dd/MM/yy}\t{task.Name}" });
			Console.WriteLine($"Task \"{task.Name}\" was added to ToDo file.");
		}

		static IEnumerable<ToDoTask> GetTasks()
		{
			if (!File.Exists(_fileName))
			{
				Console.WriteLine($"File with tasks not exists yet. Add new task at first.");
				return Enumerable.Empty<ToDoTask>();
			}

			var todoTxtLines = File.ReadAllLines(_fileName);

			if (todoTxtLines.Any())
			{
				return todoTxtLines.Select(l =>
				{
					if (ToDoTask.TryParse(l, out ToDoTask toDoTask))
					{
						return toDoTask;
					}
					else
					{
						Console.WriteLine($"Cannot parse line to Task type. Content is: {l}");
						return new NullToDoTask();
					}
				}).Where(t => t is not NullToDoTask);
			}

			return Enumerable.Empty<ToDoTask>();
		}

		static void ListTodayTasks()
		{
			var tasks = GetTasks();

			if (!tasks.Any())
			{
				Console.WriteLine("Tasks not exists. Try to add new one task at first.");
				return;
			}

			ShowTasks(tasks, DateTime.Today, DateTime.Today);
		}

		static void ListAllTasks()
		{
			var tasks = GetTasks();

			if (!tasks.Any())
			{
				Console.WriteLine("Tasks not exists. Try to add new one task at first.");
				return;
			}

			var minDate = tasks.Min(t => t.Date);
			var maxDate = tasks.Max(t => t.Date);
			ShowTasks(tasks, minDate, maxDate);
		}

		static void ShowTasks(IEnumerable<ToDoTask> tasks, DateTime from, DateTime to)
		{
			for (var date = from.Date; date < to.Date.AddDays(1); date += TimeSpan.FromDays(1))
			{
				var dateTasks = tasks.Where(t => t.Date >= date && t.Date < date.AddDays(1)).ToArray();
				if (dateTasks.Any())
				{
					Console.WriteLine($"{date:dd/MM/yy}");
					foreach (var task in dateTasks)
					{
						Console.WriteLine($"\t{task.Name}");
					}
				}
			}
		}

		static void Main(string[] args)
		{
			AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

			// понятно как работать с аргументами, но лучше переделаю на ввод параметров в командную строку			
			do
			{
				ShowMenu();
			} while (ParseParameters());
		}

		private static void ShowMenu()
		{
			//Console.Clear();
			Console.WriteLine("\n========================================");
			Console.WriteLine("Enter correct command line or type \"exit\": ");
		}

		private static bool ParseParameters()
		{
			var commandLine = Console.ReadLine().Trim();

			if (string.IsNullOrWhiteSpace(commandLine))
			{
				return ExitWithMessage(commandLine);
			}

			var commandArray = commandLine.Split(" ");

			if (!commandArray.Any())
			{
				return ExitWithMessage(commandLine);
			}

			switch (commandArray.First())
			{
				case "add":
					if (commandArray.Length == 3)
					{
						var name = commandArray[1];

						if (DateTime.TryParse(commandArray[2], out DateTime result))
						{
							AddTask(name, result);
							break;
						}
						else
						{
							return ExitWithMessage(commandLine, $"Cannot parse '{commandArray[2]}' to DateTime Format");
						}
					}
					else
					{
						return ExitWithMessage(commandLine, "Three parameter is needed for command \"add\"");
					}
				case "today":
					if (commandArray.Length == 1)
					{
						ListTodayTasks();
						break;
					}
					else
					{
						return ExitWithMessage(commandLine, "Only one parameter is needed for command \"today\"");
					}
				case "all":
					if (commandArray.Length == 1)
					{
						ListAllTasks();
						break;
					}
					else
					{
						return ExitWithMessage(commandLine, "Only one parameter is needed for command \"all\"");
					}
				case "exit":
					return false;
				default:
					return ExitWithMessage(commandLine, "Unknown command");
			}

			return true;

			bool ExitWithMessage(string inputString, string customMessage = null)
			{
				var additionalMessage = customMessage == null ? "" : $"{Environment.NewLine}{customMessage}.";

				Console.WriteLine($"---> Incorrect input ({inputString}).{additionalMessage}" +
				$"{Environment.NewLine}Press any key to try again.");
				Console.ReadKey();
				return true;
			}
		}

		private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
		{
			if (!File.Exists(_logFileName))
			{
				File.Create(_logFileName);
			}

			File.AppendAllText(_logFileName, $"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss-f-ff")}] - {ShowExceptionWithInnerIfExists((Exception)e.ExceptionObject)}");

			StringBuilder ShowExceptionWithInnerIfExists(Exception exception)
			{
				var sb = new StringBuilder();
				sb.AppendLine($"Message: {exception.Message}");
				if (exception.InnerException != null)
				{
					return sb.Append($"\t Inner {ShowExceptionWithInnerIfExists(exception.InnerException)}");
				}

				return sb;
			}
		}
	}
}
