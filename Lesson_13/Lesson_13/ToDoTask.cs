﻿using System;
using System.Globalization;

namespace Lesson_13
{
	public class ToDoTask
	{
		public DateTime Date { get; }
		public string Name { get; }

		public static readonly DateTime MinDate = new DateTime(2000, 1, 1);
		public static readonly DateTime MaxDate = new DateTime(2100, 1, 1);

		public ToDoTask(string name, DateTime date)
		{
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentException(message: "Argument is null or empty", paramName: nameof(name));
			}

			if (date > MaxDate || date < MinDate)
			{
				throw new ArgumentException(
				message: $"Date must be between {MinDate.ToShortDateString()} and {MaxDate.ToShortDateString()}",
				paramName: nameof(date));
			}

			Name = name;
			Date = date;
		}

		public static ToDoTask Parse(string input)
		{
			var split = input.Split('\t');

			if (DateTime.TryParse(split[0], out var result))
			{
				var name = split[1];
				return new ToDoTask(name, result);
			}

			return new NullToDoTask();
		}

		public static bool TryParse(string input, out ToDoTask toDoTask)
		{
			try
			{
				toDoTask = Parse(input);
				return true;
			}
			catch (FormatException)
			{
				toDoTask = new NullToDoTask();
				return false;
			}
			catch (ArgumentException)
			{
				toDoTask = new NullToDoTask();
				return false;
			}
		}

		public virtual bool IsNull() => false;
	}
}