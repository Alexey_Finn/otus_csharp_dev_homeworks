using System.Xml.Serialization;

namespace Otus.Teaching.Concurrency.Import.Handler.Entities
{
    [XmlRoot("Customer")]
    public class Customer
    {
        [XmlAttribute]
        public int Id { get; set; }

        [XmlAttribute]
        public string FullName { get; set; }

        [XmlAttribute]
        public string Email { get; set; }

        [XmlAttribute]
        public string Phone { get; set; }
		public Customer()
		{

		}
		public override string ToString()
		{
			return $"Id:{Id,5}, FullName:{FullName,20}, Phone:{Phone,15}, Email: {Email}";
		}
	}
}