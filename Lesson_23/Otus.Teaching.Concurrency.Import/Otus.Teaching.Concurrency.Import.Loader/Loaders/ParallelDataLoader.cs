﻿using Microsoft.Data.Sqlite;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
	public class ParallelDataLoader : IDataLoader
	{
		private ICustomerRepository repository;
		private CustomersList cl;

		public ParallelDataLoader(CustomersList customersList, ICustomerRepository repository)
		{
			cl = customersList;
			this.repository = repository;
		}

		public void LoadData()
		{
			const int systemPermitsCount = 64;

			while (cl.Customers.Any() || cl.Customers.Count >= systemPermitsCount)
			{
				var callBacks = new List<WaitCallback>();
				var autoResetEvents = new List<AutoResetEvent>();
				var newList = cl.Customers.Take(systemPermitsCount).ToList();
				cl.Customers.RemoveRange(0, newList.Count());
				RunThreadPool(newList, callBacks, autoResetEvents);
			}

			Console.WriteLine("All calculations are complete.");

			void RunThreadPool(List<Customer> listOfCustomers, List<WaitCallback> callBacks, List<AutoResetEvent> autoResetEvents)
			{
				listOfCustomers.ForEach(customer =>
				{
					var are = new AutoResetEvent(false);
					var pdo = new ParallelDataLoaderObject(repository, are, customer);
					var wcb = new WaitCallback(pdo.AddCustomer);
					ThreadPool.QueueUserWorkItem(wcb, customer);
					callBacks.Add(wcb);
					autoResetEvents.Add(are);
				});

				WaitHandle.WaitAll(autoResetEvents.ToArray());
			}


			//var parallelLoopResult = Parallel.ForEach(cl.Customers, c => repository.AddCustomer(c));
		}

		public void ReadData()
		{
			repository.ReadDatabase();
		}

		private class ParallelDataLoaderObject
		{
			AutoResetEvent autoResetEvent;
			ICustomerRepository repository;
			Customer customer;

			public ParallelDataLoaderObject(ICustomerRepository customerRepository, AutoResetEvent autoResetEvent, Customer customer)
			{
				this.repository = customerRepository;
				this.autoResetEvent = autoResetEvent;
				this.customer = customer;
			}
			public void AddCustomer(object state)
			{
				repository.AddCustomer(customer);
				autoResetEvent.Set();
			}
		}
	}
}
