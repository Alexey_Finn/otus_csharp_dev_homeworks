﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Loaders;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace ParallelLoadDataFromFile
{
	class Program
	{
		private static string DataFile;
		private static CustomersList cl = new();
		private static IDataParser<List<Customer>> parser;
		private static IDataLoader loader;

		static void Main(string[] args)
		{
			Console.WriteLine($"Main program started with process Id={Environment.ProcessId}...");
			
			if (args.Length == 0)
			{
				// for testing
				args = new string[] { "/process", "/count", "-100" };
			}

			GenerateCustomersList(args);
			DeserializeData(DataFile);
			LoadDataToDb(cl, new CustomerRepository());

			Console.ReadLine();
		}

		private static void GenerateCustomersList(string[] args)
		{
			var timeStamp = DateTime.Now;
			Console.WriteLine($"GenerateCustomersList started at {timeStamp}");
			var loaderProjectPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			var loaderResultDirectory = Path.GetFullPath(Path.Combine(loaderProjectPath, @"..\..\..\result"));
			var loaderBinaryPath = Path.GetFullPath(Path.Combine(loaderProjectPath, @"..\..\..\..\Otus.Teaching.Concurrency.Import.Loader\bin\"));
			var loaderExeFileTemplate = "Otus.Teaching.Concurrency.Import.Loader.exe";
			var customerXmlFile = "customers.xml";
			var startNewGeneratorProcess = false;
			var countOfRecords = 10;

			if (args.Length != 0)
			{
				var indexOfNewProcessMode = args.ToList().LastIndexOf("/process");
				var indexOfCount = args.ToList().LastIndexOf("/count");

				if (indexOfNewProcessMode != -1)
				{
					startNewGeneratorProcess = true;
				}

				if (indexOfCount != -1 && int.TryParse(args[indexOfCount + 1].Replace("-", ""), out var result))
				{
					countOfRecords = result;
				}
			}

			if (startNewGeneratorProcess)
			{
				var loaderFile = GetFullFilePath(loaderBinaryPath, loaderExeFileTemplate);

				if (loaderFile == null)
				{
					throw new Exception($"'{loaderExeFileTemplate}' not exists under '{loaderBinaryPath}' directory");
				}

				var processInfo = new ProcessStartInfo()
				{
					FileName = loaderFile,
					RedirectStandardOutput = true,
					Arguments = string.Join(" ", args.Append("/path").Append(loaderResultDirectory))
				};

				var process = new Process()
				{
					StartInfo = processInfo
				};
				var processTimeStamp = DateTime.Now;
				Console.WriteLine($"Inner process run at {processTimeStamp}");
				process.Start();

				var output = process.StandardOutput.ReadToEnd();
				process.WaitForExit();
				Console.WriteLine($"Process exited at {DateTime.Now} after {(DateTime.Now - processTimeStamp).TotalSeconds} seconds");

				Console.WriteLine($"" +
				$"process.ExitCode = {process.ExitCode}\n" +
				$"standart ouput begin =>\n\t{output.Replace("\r\n", "\r\n\t")}\r" +
				$"=> standart ouput finish.");
			}
			else
			{
				Otus.Teaching.Concurrency.Import.Loader.Program.GenerateCustomersDataFile(countOfRecords);
			}
			
			DataFile = Path.Combine(loaderResultDirectory, customerXmlFile);
			File.Copy(GetFullFilePath(loaderBinaryPath, customerXmlFile), DataFile, true);
			Console.WriteLine($"GenerateCustomersList finished at {DateTime.Now} after {(DateTime.Now - timeStamp).TotalSeconds} seconds" +
				$"");
		}
		private static void DeserializeData(string resourceFilePath)
		{
			parser = new XmlParser(resourceFilePath);
			cl.Customers = parser.Parse();

			Console.WriteLine("Deserialized data:");
			cl.Customers.ForEach(c => Console.WriteLine(c));
		}

		private static void LoadDataToDb(CustomersList cl, ICustomerRepository repository)
		{
			loader = new ParallelDataLoader(cl, repository);
			loader.LoadData();

			CustomerRepository cr = new CustomerRepository();
			cr.ReadDatabase();
		}

		private static string GetFullFilePath(string pathToStartSearching, string searchTemplate)
		{
			var fileSystemEntries = Directory.EnumerateFileSystemEntries(pathToStartSearching);
			var result = string.Empty;

			foreach (var entry in fileSystemEntries)
			{
				if (Path.GetFileName(entry).Equals(searchTemplate))
				{
					return entry;
				}
				else if (File.GetAttributes(entry).HasFlag(FileAttributes.Directory))
				{
					result = GetFullFilePath(entry, searchTemplate);
					break;
				}
			}

			if (string.IsNullOrEmpty(result))
			{
				throw new Exception($"Cannot find file '{searchTemplate}' under '{pathToStartSearching}' directory.");
			}

			return result;
		}
	}
}
