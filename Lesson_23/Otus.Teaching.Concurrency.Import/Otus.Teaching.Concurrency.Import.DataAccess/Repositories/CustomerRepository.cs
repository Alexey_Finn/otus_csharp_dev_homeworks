using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Dapper;
using Microsoft.Data.Sqlite;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
	public class CustomerRepository
		: ICustomerRepository
	{
		private readonly static object _lock = new object();
		private static string DbFile
		{
			get { return Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\result", "customersdata.db"); }
		}

		public CustomerRepository()
		{
			lock (_lock)
			{
				if (!File.Exists(DbFile))
				{
					CreateDatebase();
				}
			}
		}

		private static SqliteConnection MakeConnection()
		{
			return new SqliteConnection("Data Source=" + DbFile);
		}

		public void AddCustomer(Customer customer)
		{
			//Add customer to data source

			Console.WriteLine($"==> Execution of \"AddCutomer\" has begun at thread {Thread.CurrentThread.ManagedThreadId}");

			using var connection = MakeConnection();
			connection.Open();
			try
			{
				connection.Execute(
@"INSERT INTO Customer
    ( Id, FullName, Email, Phone ) VALUES
    ( @Id, @FullName, @Email, @Phone );", customer);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}

			Console.WriteLine($"Inserted record to db");
		}

		public void ReadDatabase()
		{
			lock (_lock)
			{
				if (!File.Exists(DbFile)) return;
			}

			List<Customer> results;

			using (var cnn = MakeConnection())
			{
				cnn.Open();
				results = cnn.Query<Customer>(@"SELECT Id, FullName, Phone, Email FROM Customer ORDER BY Id").AsList();
			}

			Console.WriteLine("Database reading result:");
			foreach (var item in results)
			{
				Console.WriteLine(item);
			}
			Console.WriteLine($"Total {results.Count} records was readed.");
		}

		private static void CreateDatebase()
		{
			using (var connection = MakeConnection())
			{
				connection.Open();
				connection.Execute(
@"create table Customer
(
    Id         INTEGER IDENTITY PRIMARY KEY,
    FullName   varchar(100) not null,
    Email      varchar(100) not null,
    Phone      varchar(100) not null
)");
			}
		}
	}
}