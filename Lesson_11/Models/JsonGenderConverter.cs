﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Lesson_11.Models
{
	public class JsonGenderConverter : JsonConverter<Gender>
	{
		public override Gender Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			throw new NotImplementedException();
		}

		public override void Write(Utf8JsonWriter writer, Gender value, JsonSerializerOptions options)
		{
			switch (value)
			{
				case Gender.None:
					writer.WriteStringValue("n_json");
					break;
				case Gender.Male:
					writer.WriteStringValue("m_json");
					break;
				case Gender.Female:
					writer.WriteStringValue("f_json");
					break;
				default:
					break;
			}
		}
	}
}
