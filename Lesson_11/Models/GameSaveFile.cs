using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;
using System.Xml.Serialization;

namespace Lesson_11.Models
{
	[JsonConverter(typeof(JsonGenderConverter))]
	public enum Gender
	{
		[XmlEnum("n_xml")] None = 0, 
		[XmlEnum("m_xml")] Male = 1,
		[XmlEnum("f_xml")] Female = 2,
	}

	/// <summary>
	/// Предмет
	/// </summary>
	[Serializable]
	public class Item : ISerializable
	{
		public Item()
		{

		}

		/// <summary>
		/// Название предмета
		/// </summary>
		/// <value></value>
		public string Name { get; set; }

		/// <summary>
		/// Количество
		/// </summary>
		/// <value></value>
		[JsonConverter(typeof(JsonQuantityConverter))]
		public int Quantity { get; set; }

		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			const string paramName = "add_quantity";
			info.AddValue(paramName, Quantity);
			var val = info.GetInt32(paramName);

			if (val < 0)
				throw new Exception(
					$"Value must be greater than zero. Value: {val}",
					new ArgumentOutOfRangeException(nameof(Quantity)));
		}

		public override string ToString()
		{
			return $"{{Name: {Name}, Quantity: {Quantity}}}";
		}
	}

	/// <summary>
	/// Информация о пользователе
	/// </summary>
	[Serializable]
	public class User
	{
		/// <summary>
		/// Уровень пользователя
		/// </summary>
		/// <value></value>
		[XmlAttribute]
		public int Level { get; set; }

		/// <summary>
		/// Имя
		/// </summary>
		/// <value></value>
		public string Name { get; set; }

		/// <summary>
		/// Пол персонажа
		/// </summary>
		/// <value></value>
		public Gender Gender { get; set; }

		public override string ToString()
		{
			return $"{{ Gender: {Gender}, Name: {Name}, Level: {Level} }}";
		}
	}

	/// <summary>
	/// Состояние игры
	/// </summary>
	[Serializable]
	public class GameStatus
	{
		/// <summary>
		/// Текущая локация
		/// </summary>
		/// <value></value>
		public string CurrentLocation { get; set; }

		/// <summary>
		/// Информация о пользователе
		/// </summary>
		/// <value></value>
		[XmlElement("u")]
		[JsonPropertyName("u")]
		public User User { get; set; }

		public Item[] Items { get; set; }

		/// <summary>
		/// Координаты пользователя
		/// </summary>
		/// <value></value>
		[XmlIgnore]
		[JsonIgnore]
		public (double, double) Coords { get; set; }

		public override string ToString()
		{
			return $"{{User: {User}, CurrentLocation: {CurrentLocation}, Coords: {Coords}, Items: [{Items.Select(x => x.ToString())}]}}";
		}
	}

	/// <summary>
	/// Сохраняемый файл
	/// </summary>
	[Serializable]
	public class SaveFile : GameStatus
	{
		/// <summary>
		/// Дата создания файла
		/// </summary>
		/// <value></value>
		public DateTime CreatedDate { get; set; }

		/// <summary>
		/// Дата сохранения
		/// </summary>
		/// <value></value>
		public DateTime? SaveDate { get; set; }

		/// <summary>
		/// НАзвание файла
		/// </summary>
		/// <value></value>
		public string FileName { get; set; }

		public override string ToString()
		{
			return $"{{FileName: {FileName}, SaveDate: {SaveDate}, CreatedDate: {CreatedDate}, GameStatus:  {base.ToString()} }}";
		}
	}
}