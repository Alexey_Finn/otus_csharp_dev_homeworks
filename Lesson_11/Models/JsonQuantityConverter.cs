﻿using System;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Lesson_11.Models
{
	public class JsonQuantityConverter : JsonConverter<int>
	{
		public override int Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
		{
			throw new NotImplementedException();
		}

		public override void Write(Utf8JsonWriter writer, int value, JsonSerializerOptions options)
		{
			if (value < 0)
			{
				throw new Exception(
					message: $"Value must be greater than zero. Value: {value}",
					new ArgumentOutOfRangeException());
			}
		}
	}
}
