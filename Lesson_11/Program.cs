﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
using System.Xml.Serialization;
using Lesson_11.Models;

namespace Lesson_11
{
	class Program
	{
		static SaveFile Generate1()
		{
			return new SaveFile
			{
				Coords = (1241.44, 124145.4),
				CurrentLocation = "Dungeon",
				User = new User { Level = 10, Name = "Пушкин", Gender = Gender.Male },
				Items = new[] { 
					new Item { Name = "Топор", Quantity = 2 },
					new Item { Name = "weapon12", Quantity = 15 },
					new Item { Name = "Торт", Quantity = 4 }},
				CreatedDate = DateTime.Now.AddYears(-5),
				SaveDate = DateTime.UtcNow,
				FileName = "randomFile_13"
			};
		}

		static SaveFile Generate2()
		{
			return new SaveFile
			{
				Coords = (121.44, 124.4),
				CurrentLocation = "Subway",
				User = new User { Level = 10, Name = "Feodorov", Gender = Gender.Female },
				Items = new[] {
					new Item { Name = "ddddd", Quantity = 11 },
					new Item { Name = "qwwefff", Quantity = 667 },
					new Item { Name = "увцкмукмйййёёё", Quantity = -2 }},
				CreatedDate = DateTime.Now.AddYears(-6),
				SaveDate = DateTime.UtcNow,
				FileName = "randomFile_24"
			};
		}

		static void SerializeBinary(SaveFile[] sf)
		{
			var serializer = new BinaryFormatter();
			using var fs = new FileStream("bin.bin", FileMode.Create);

			serializer.Serialize(fs, sf);
		}

		static void SerializeJson(SaveFile[] sf)
		{
			var jso = new JsonSerializerOptions
			{
				Encoder = JavaScriptEncoder.Create(UnicodeRanges.All),
				WriteIndented = true
			};

			var json = JsonSerializer.Serialize(sf, jso);
			File.WriteAllText("json.json", json);
		}

		static void SerializeXml(SaveFile[] sf)
		{
			var serializer = new XmlSerializer(typeof(SaveFile[]));
			using var fs = new FileStream("xml.xml", FileMode.Create);

			serializer.Serialize(fs, sf);
		}

		static void Main(string[] args)
		{
			var g1 = Generate1();
			var g2 = Generate2();

			SerializeBinary(new[] { g1, g2 });
			SerializeJson(new[] { g1, g2 });
			SerializeXml(new[] { g1, g2 });
		}
	}
}