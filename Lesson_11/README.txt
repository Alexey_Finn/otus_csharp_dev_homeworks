1. XML, JSON, Binary - сериализовать			+
2. XML, JSON Пользователь сохраняется как "u"	+
3. XML: User.level как атрибут					+
4. Coords - не сериализовать					+

Кто успел 1-4,
5. JSON: Кириллица - сериализовать				+
6. JSON, XML: сериализовать Gender как m/f		+

Со звездочкой
7. Items.Quantity - если меньше 0 - Exception	+

-------------------------------------------------

Результаты сериализации:

json.json
[
  {
    "CreatedDate": "2016-06-11T00:21:14.2719307+03:00",
    "SaveDate": "2021-06-10T21:21:14.2729926Z",
    "FileName": "randomFile_13",
    "CurrentLocation": "Dungeon",
    "u": {
      "Level": 10,
      "Name": "Пушкин",
      "Gender": "m_json"
    },
    "Items": [
      {
        "Name": "Топор",
        "Quantity": 2
      },
      {
        "Name": "weapon12",
        "Quantity": 15
      },
      {
        "Name": "Торт",
        "Quantity": 4
      }
    ]
  },
  {
    "CreatedDate": "2015-06-11T00:21:14.2733105+03:00",
    "SaveDate": "2021-06-10T21:21:14.2733119Z",
    "FileName": "randomFile_24",
    "CurrentLocation": "Subway",
    "u": {
      "Level": 10,
      "Name": "Feodorov",
      "Gender": "f_json"
    },
    "Items": [
      {
        "Name": "ddddd",
        "Quantity": 11
      },
      {
        "Name": "qwwefff",
        "Quantity": 667
      },
      {
        "Name": "увцкмукмйййёёё",
        "Quantity": -2
      }
    ]
  }
]

xml.xml
<?xml version="1.0"?>
<ArrayOfSaveFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <SaveFile>
    <CurrentLocation>Dungeon</CurrentLocation>
    <u Level="10">
      <Name>Пушкин</Name>
      <Gender>m_xml</Gender>
    </u>
    <Items>
      <Item>
        <Name>Топор</Name>
        <Quantity>2</Quantity>
      </Item>
      <Item>
        <Name>weapon12</Name>
        <Quantity>15</Quantity>
      </Item>
      <Item>
        <Name>Торт</Name>
        <Quantity>4</Quantity>
      </Item>
    </Items>
    <CreatedDate>2016-06-11T00:21:14.2719307+03:00</CreatedDate>
    <SaveDate>2021-06-10T21:21:14.2729926Z</SaveDate>
    <FileName>randomFile_13</FileName>
  </SaveFile>
  <SaveFile>
    <CurrentLocation>Subway</CurrentLocation>
    <u Level="10">
      <Name>Feodorov</Name>
      <Gender>f_xml</Gender>
    </u>
    <Items>
      <Item>
        <Name>ddddd</Name>
        <Quantity>11</Quantity>
      </Item>
      <Item>
        <Name>qwwefff</Name>
        <Quantity>667</Quantity>
      </Item>
      <Item>
        <Name>увцкмукмйййёёё</Name>
        <Quantity>-2</Quantity>
      </Item>
    </Items>
    <CreatedDate>2015-06-11T00:21:14.2733105+03:00</CreatedDate>
    <SaveDate>2021-06-10T21:21:14.2733119Z</SaveDate>
    <FileName>randomFile_24</FileName>
  </SaveFile>
</ArrayOfSaveFile>