﻿Формат интерактива
Все будут распределены по командам;
Один участник команды расшаривает экран и совместными усилиями выполняется задание;
По окончанию времени, команда расшаривает экран и показывает что успели сделать за отведенное время.

Задание:
Создать атрибут LastChangeAttribute, хранящий данные о последнем изменении класса, включающем имя изменившего, 
время изменения (СТРОКОЙ) и номер задачи в рамках которой происходило изменение. 
Получить данные из атрибута.

[LastChange("Alex", "2020-10-02", 11576)]
public class MyClass {}



Пример создания атрибута для примечаний
[AttributeUsage(AttributeTargets.All)]
public class RemarkAttribute : Attribute
{
	string _remark;

	public RemarkAttribute(string comment)
	{
		_remark = comment;
	}

	public string Remark
	{
		get { return _remark; }
	}
}

Пример использования атрибута
[RemarkAttribute("This class uses an attribute.")]
class UseAttribute { }

Пример проверки наличия атрибута
var isAttributeDefined = typeof(UseAttribute).IsDefined(typeof(RemarkAttribute));

3 примера получения данных из атрибута
string remark;
remark = (typeof(UseAttribute).GetCustomAttribute(typeof(RemarkAttribute)) as RemarkAttribute).Remark;
remark = (typeof(UseAttribute).GetCustomAttributes(typeof(RemarkAttribute)).Single() as RemarkAttribute).Remark;
remark = typeof(UseAttribute).GetCustomAttributesData().Single(x => x.AttributeType == typeof(RemarkAttribute)).ConstructorArguments.First().Value.ToString();
