﻿using Prototype.Entities;
using Prototype.Entities.BaseEntities;
using System;
using System.Linq;

namespace Practice
{
	class Program
	{
		static void Main(string[] args)
		{
			// Create unit of class Militia
			var militia1 = new Militia("Ivan");
			// Create clone of unit
			var militia2 = militia1.CloneMy();
			var militia3 = militia1.CloneMy();

			ShowMembers(militia1, militia2, militia3);

			// next block demostrate that properties of instance and their clone are independent
			militia1.Hit(militia2);
			militia1.Hit(militia2);
			militia1.Hit(militia2);

			militia1.LevelUp();
			militia1.LevelUp();

			militia1.Hit(militia2);
			militia1.Hit(militia2);
			militia1.Hit(militia2);

			militia2.LevelUp();
			militia2.LevelUp();
			militia2.LevelUp();
			militia2.LevelUp();

			militia2.Hit(militia1);
			militia2.Hit(militia1);

			ShowMembers(militia1, militia2);

			// create unit of class TrainedMilitia
			var trained1 = new TrainedMilitia("Stepan");
			// call multiple clone methods
			var trained2 = trained1.CloneMy();
			var militia4 = militia2.CloneMy();
			var militia5 = militia1.CloneMy();

			// shows, that copy() on different classe will return correct class
			ShowMembers(militia1, militia2, militia3, trained1, trained2, militia4, militia5);

			militia4.Hit(militia3);
			militia5.Hit(militia3);
			trained1.Hit(militia3);
			trained2.Hit(militia3);
			militia1.Hit(militia3);
			militia2.Hit(militia3);

			ShowMembers(militia1, militia2, militia3, trained1, trained2, militia4, militia5);

			Console.WriteLine($"{militia5.Name} is clone of {militia1.Name}");
			Console.WriteLine($"Lets {militia1.Name} will get damage and {militia5.Name} will LevelUp");
			Console.WriteLine();
			trained1.Hit(militia1);
			militia5.LevelUp();

			Console.WriteLine();
			Console.Write($"Okay. Lets check result");
			// demonstrate that properties of copied and original object are independent.
			ShowMembers(militia1, militia5);


			Console.WriteLine($"Lets clone {militia1.Name} and {trained1.Name} with ICloneable.Clone()");
			Console.WriteLine();
			var clone1 = (Militia)militia1.Clone();
			var clone2 = (TrainedMilitia)trained1.Clone();

			Console.WriteLine();
			Console.Write($"Lets see the result");
			ShowMembers(militia1, clone1, trained1, clone2);

			Console.WriteLine($"Try to change parameters of clones");
			Console.WriteLine();
			militia1.Hit(clone1);
			trained1.Hit(clone2);
			clone1.LevelUp();
			clone2.LevelUp();

			Console.WriteLine();
			Console.Write($"Check the results");
			ShowMembers(militia1, clone1, trained1, clone2);


			Console.ReadKey();
		}

		private static void ShowMembers(params Unit[] units)
		{
			Console.WriteLine($"\n=========================");
			units.ToList().ForEach(e => Console.WriteLine(e));
			Console.WriteLine($"\n=========================");
		}
	}
}
