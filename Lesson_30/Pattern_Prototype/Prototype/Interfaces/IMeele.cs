﻿using Prototype.Entities.BaseEntities;

namespace Prototype.Interfaces
{
	/// <summary>
	/// For battle units that has meele attack
	/// </summary>
	public interface IMeele : IFighter
	{
		void Hit(IFighter unit);
	}
}
