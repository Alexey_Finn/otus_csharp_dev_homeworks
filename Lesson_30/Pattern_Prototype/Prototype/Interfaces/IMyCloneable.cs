﻿namespace Prototype.Interfaces
{
	/// <summary>
	/// Custom interface for realization of method Clone()
	/// </summary>
	/// <typeparam name="T">Which class will be uses to clone</typeparam>
	public interface IMyCloneable<T>
	{
		/// <summary>
		/// Do clone of object with class specified.
		/// </summary>
		/// <returns>Object, that contain copied properties, 
		/// but all of  this properties will be independent.</returns>
		T CloneMy();
	}
}
