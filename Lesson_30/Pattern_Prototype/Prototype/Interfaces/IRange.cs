﻿namespace Prototype.Interfaces
{
	/// <summary>
	/// For battle units that has ranged attack
	/// </summary>
	public interface IRange : IFighter
	{
		void Shoot(int damage);
	}
}