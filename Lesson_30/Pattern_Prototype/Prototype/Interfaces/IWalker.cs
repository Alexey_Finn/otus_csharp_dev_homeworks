﻿namespace Prototype.Interfaces
{
	/// <summary>
	/// For units that can walk on the ground
	/// </summary>
	interface IWalker
	{
		/// <summary>
		/// Do movement of unit on the ground
		/// </summary>
		/// <param name="range">Dustance for movement in meters.</param>
		void Walk(int range);
	}
}
