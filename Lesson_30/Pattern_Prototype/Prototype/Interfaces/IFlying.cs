﻿namespace Prototype.Interfaces
{
	/// <summary>
	/// For objects, that can fly.
	/// </summary>
	interface IFlying
	{
		void Fly(int range);
	}
}