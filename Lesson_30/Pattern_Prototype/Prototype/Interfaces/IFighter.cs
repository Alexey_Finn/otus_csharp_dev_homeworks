﻿namespace Prototype.Interfaces
{
	/// <summary>
	/// Base realization of objects, that have battle properties.
	/// </summary>
	public interface IFighter
	{
		float AttackKoef { get; set; }
		float Defensekoef { get; set; }
		int Attack { get; set; }
		int Defense { get; set; }
		void GetDamage(int damage);
	}
}