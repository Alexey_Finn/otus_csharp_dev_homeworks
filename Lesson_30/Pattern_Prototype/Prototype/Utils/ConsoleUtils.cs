﻿using System;

namespace Prototype.Utils
{
	public static class ConsoleUtils
	{
		public static void WriteLineGreen(string text)
		{
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine(text);
			Console.ResetColor();
		}

		public static void WriteLineYellow(string text)
		{
			Console.ForegroundColor = ConsoleColor.DarkYellow;
			Console.WriteLine(text);
			Console.ResetColor();
		}

		public static void WriteLineRed(string text)
		{
			Console.ForegroundColor = ConsoleColor.Red;
			Console.WriteLine(text);
			Console.ResetColor();
		}
	}
}
