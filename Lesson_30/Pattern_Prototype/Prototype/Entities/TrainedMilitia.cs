﻿using System;

namespace Prototype.Entities
{
	/// <summary>
	/// Game unit. Better version of Militia class
	/// </summary>
	public class TrainedMilitia : Militia
	{
		public TrainedMilitia(string name) : base(name)
		{
			Description = "Trained Militia warrior";
			Weapon = "cleaver";
			AttackKoef = 2f;
			AttackKoef = 2f;
			Defensekoef = 2f;
		}

		/// <summary>
		/// Prototype constructor
		/// </summary>
		/// <param name="forClone">Object for clone.</param>
		public TrainedMilitia(Militia forClone) : base(forClone)
		{
			Description = "Trained Militia warrior";
			Weapon = "cleaver";
			AttackKoef = 2f;
			AttackKoef = 2f;
			Defensekoef = 2f;
		}

		/// <summary>
		/// IMyCloneable<T> realization.
		/// </summary>
		/// <returns>Clone of object. Has independet parameters and properties.</returns>
		public override TrainedMilitia CloneMy()
		{
			var trainedMilitia = new TrainedMilitia(this);
			Console.WriteLine($"{Name} was copied!");
			return trainedMilitia;
		}
	}
}