﻿using Prototype.Entities.BaseEntities;
using Prototype.Interfaces;
using System;

namespace Prototype.Entities
{
	/// <summary>
	/// Game unit, that can join the battle, has meele range and can wal on ground
	/// </summary>
	public class Militia : MeeleFighter, IWalker
	{
		public Militia(string name) : base(name)
		{
			Description = "Militia warrior";
			Weapon = "spade";
			Armor = "light jacket";
			AttackKoef = 1.5f;
			Defensekoef = 1.5f;
		}

		/// <summary>
		/// Prototype constructor
		/// </summary>
		/// <param name="forClone">Object for clone.</param>
		public Militia(Militia forClone) : base(forClone)
		{
			Description = "Militia warrior";
			Weapon = "spade";
			Armor = "light jacket";
			AttackKoef = 1.5f;
			Defensekoef = 1.5f;
		}
		/// <summary>
		/// Interface IMyCloneable<T> realization. Calls to prototype constructor.
		/// </summary>
		/// <returns>Clone of object, that has independent parameters and properties.</returns>
		public override Militia CloneMy()
		{
			var militia = new Militia(this);
			Console.WriteLine($"{Name} was copied!");
			return militia;
		}

		public void Walk(int range)
		{
			Console.WriteLine($"\"{this}\" walks to {range} km");
		}
	}
}