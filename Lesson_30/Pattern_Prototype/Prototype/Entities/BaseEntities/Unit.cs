﻿using Prototype.Interfaces;
using Prototype.Utils;
using System;
using System.Threading;

namespace Prototype.Entities.BaseEntities
{
	/// <summary>
	/// Base Unit. Contains common properties of all possible derived objects
	/// </summary>
	public abstract class Unit : IMyCloneable<Unit>, ICloneable
	{
		/// <summary>
		/// global counter to recognize different units
		/// </summary>
		private static int counter = 0;
		private bool isAlive;

		delegate void VitalityHandler(Unit unit);
		public string Name { get; set; }
		public uint Level { get; set; }
		public int FullHealth { get; set; }
		public int CurrentHealth { get; set; }
		public string Description { get; set; } = "Base unit";
		public bool IsAlive
		{
			get => isAlive; set
			{
				if (!value)	NotAlive?.Invoke(this);				
				isAlive = value;
			}
		}

		private event VitalityHandler NotAlive;

		public Unit(string name)
		{
			Name = name.Split('_')[0] + "_" + Interlocked.Increment(ref counter);
			Level = 1;
			FullHealth = 100;
			CurrentHealth = 100;
			IsAlive = true;
			NotAlive += Notify;
			ConsoleUtils.WriteLineGreen($"{this} was spawned!");
		}

		/// <summary>
		/// Prototype constructor of class
		/// </summary>
		/// <param name="forClone">Object for clone</param>
		public Unit(Unit forClone) : this(forClone.Name)
		{
			Level = forClone.Level;
			FullHealth = forClone.FullHealth;
			CurrentHealth = forClone.CurrentHealth;
			Description = forClone.Description;
			IsAlive = forClone.IsAlive;
		}

		/// <summary>
		/// Method for get clone of object. Will be realized in concrete classes.
		/// </summary>
		/// <returns>Clone of object.</returns>
		public abstract Unit CloneMy();

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public object Clone()
		{
			return CloneMy();
		}

		public override string ToString()
		{
			return $"{Description} \"{Name}\" [Lvl_{Level}] ({CurrentHealth}/{FullHealth})";
		}

		/// <summary>
		/// Method to notify about "unit is dead" event. Only one function
		/// </summary>
		/// <param name="unit">Instance that refers to event</param>
		private static void Notify(Unit unit)
		{
			ConsoleUtils.WriteLineRed($"{unit} is dead");
		}

		/// <summary>
		/// Method to emulate LevelUp of unit. Do nothing for not alive units (simplicity).
		/// </summary>
		public void LevelUp()
		{
			if (!IsAlive) return;
			Level++;
			FullHealth += FullHealth / 10;
			ConsoleUtils.WriteLineYellow($"{this} leveled up!");
		}
	}
}