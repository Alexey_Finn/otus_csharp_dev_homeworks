﻿using Prototype.Interfaces;
using Prototype.Utils;

namespace Prototype.Entities.BaseEntities
{
	/// <summary>
	/// Base class for units, that can join battle and has meele range
	/// </summary>
	public abstract class MeeleFighter : Unit, IMeele
	{
		private int attack;
		private int defense;

		/// <summary>
		/// Property to calculate and use offensive property of unit
		/// </summary>
		public int Attack
		{
			get
			{
				var _attack = (int)((attack + Level) * AttackKoef);
				return _attack <= 0 ? 1 : _attack;
			}

			set => attack = value <= 0 ? 1 : value;
		}
		/// <summary>
		/// Property to calculate and use defense property of unit
		/// </summary>
		public int Defense
		{
			get
			{
				var _defense = (int)((defense + Level) * Defensekoef);
				return _defense <= 0 ? 1 : _defense >= 100 ? 99 : _defense;
			}

			set => defense = value <= 0 ? 1 : value >= 100 ? 99 : value;
		}
		/// <summary>
		/// Weapon property. Can increase Defense. Just string with describe. Should be separate class
		/// </summary>
		public virtual string Weapon { get; set; }
		/// <summary>
		/// Armor property. Can decrease incoming damage. Just string with describe. Should be separate class
		/// </summary>
		public virtual string Armor { get; set; }
		/// <summary>
		/// Property for multiply Attack.
		/// </summary>
		public float AttackKoef { get; set; } = 1f;
		/// <summary>
		/// Property for multiply Defense.
		/// </summary>
		public float Defensekoef { get; set; } = 1f;

		protected MeeleFighter(string name) : base(name)
		{
			Attack = 10;
			Defense = 10;
		}

		/// <summary>
		/// Prototype constructor.
		/// </summary>
		/// <param name="forClone">Object for clone.</param>
		protected MeeleFighter(MeeleFighter forClone) : base(forClone)
		{
			Attack = forClone.Attack;
			Defense = forClone.Defense;
			Weapon = forClone.Weapon;
			Armor = forClone.Armor;
		}

		/// <summary>
		/// Method to calculate incoming damage
		/// </summary>
		/// <param name="damage">integer value of damage (before defense).</param>
		public void GetDamage(int damage)
		{
			if (!IsAlive)
			{
				ConsoleUtils.WriteLineRed($"{Name} already dead.");
				return;
			}

			var currentDamage = damage - damage * defense / 100;

			ConsoleUtils.WriteLineRed($"{Name} taken {currentDamage} damage.");
			CurrentHealth -= currentDamage < 0 ? 0 : currentDamage;
			if (CurrentHealth <= 0)
			{
				IsAlive = false;
			}
		}

		/// <summary>
		/// Method to do damage for other unit
		/// </summary>
		/// <param name="unit">Object that will get damage.</param>
		public void Hit(IFighter unit)
		{
			System.Console.WriteLine($"{this.Name} trying to hit target with {Attack} force");
			unit.GetDamage(Attack);
		}

		public override string ToString()
		{
			return base.ToString() + " " + $"with {Weapon} in hand and dressed in {Armor}";
		}
	}
}