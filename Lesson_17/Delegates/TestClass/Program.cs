﻿using ClassLibrary;
using System;

namespace TestClass
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Program started.\n");	

			var dr = new DocumentsReceiver();
			dr.Start("TestDir", TimeSpan.FromMinutes(1));

			Console.WriteLine($"\nYou should place documents to this folder: {dr.TargetDirectory}");
			Console.WriteLine("--- Waiting for documents or timeout ---");
			Console.ReadLine();
			Console.WriteLine("\nProgram finished.");
		}		
	}
}
