﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Timers;

namespace ClassLibrary
{
	public class DocumentsReceiver
	{
		private const string PassportTemplate = "Паспорт.jpg";
		private const string IssueTemplate = "Заявление.txt";
		private const string PhotoTemplate = "Фото.jpg";
		private string _targetDirectory;
		private Timer Timer;
		private FileSystemWatcher Fsw;
		private bool _isStarted = false;

		public string TargetDirectory
		{
			get { return _targetDirectory; }
			private set
			{	
				if (!Directory.Exists(value))
				{
					Directory.CreateDirectory(value);
				}
				_targetDirectory = value;
			}
		}

		public event FileSystemEventHandler DocumentsReady;
		public event ElapsedEventHandler TimedOut;

		public void Start(string targetDirectory, TimeSpan waitingInterval)
		{
			Console.WriteLine($"Method '{MethodBase.GetCurrentMethod().Name}' is started.");

			_isStarted = true;
			DocumentsReady += DocumentsReceiver_DocumentsReady;
			TimedOut += TimedOutEventHandler;

			TargetDirectory = targetDirectory;

			Timer = new Timer(waitingInterval.TotalMilliseconds);
			Timer.Elapsed += Timer_Elapsed;
			Fsw = new FileSystemWatcher(targetDirectory);
			Fsw.Changed += Fsw_Changed;
			Fsw.Created += Fsw_Changed;
			Fsw.Deleted += Fsw_Changed;
			Fsw.Renamed += Fsw_Changed;

			Fsw.EnableRaisingEvents = true;
			Timer.Start();

			Console.WriteLine($"Method '{MethodBase.GetCurrentMethod().Name}' is finished.");
		}

		public void Stop()
		{
			Console.WriteLine($"Method '{MethodBase.GetCurrentMethod().Name}' is started.");

			if (_isStarted)
			{
				Timer.Stop();
				Fsw.Dispose();
				DocumentsReady -= DocumentsReceiver_DocumentsReady;
				TimedOut -= TimedOutEventHandler;
				_isStarted = false;
			}

			Console.WriteLine($"Method '{MethodBase.GetCurrentMethod().Name}' is finished.");
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			Timer.Stop();
			Fsw.Dispose();
			Console.WriteLine($"[Log] Event handler '{MethodBase.GetCurrentMethod().Name}' triggered.");
			TimedOut?.Invoke(sender, e);

			Console.WriteLine($"Time is over. Estimated {((Timer)sender).Interval} ms");
		}

		private void Fsw_Changed(object sender, FileSystemEventArgs e)
		{
			Console.WriteLine($"[Log] Event handler '{MethodBase.GetCurrentMethod().Name}' triggered.");
			var files = new DirectoryInfo(TargetDirectory).EnumerateFiles().Select(e => e.Name);
			if (files.Contains(PassportTemplate) && files.Contains(IssueTemplate) && files.Contains(PhotoTemplate))
			{
				Timer.Stop();
				Fsw.Dispose();
				DocumentsReady?.Invoke(sender, e);
				Console.WriteLine("All documents ready!");
			}
		}

		private void TimedOutEventHandler(object sender, ElapsedEventArgs e)
		{
			Console.WriteLine("[Log] TimedOut event triggered!");
		}

		private void DocumentsReceiver_DocumentsReady(object sender, FileSystemEventArgs e)
		{
			Console.WriteLine("[Log] DocumentsReady event triggered!");
		}

		~DocumentsReceiver()
		{
			Console.WriteLine($"[Log] Destructor '{MethodBase.GetCurrentMethod().Name}' triggered.");
			TimedOut -= TimedOutEventHandler;
			DocumentsReady -= DocumentsReceiver_DocumentsReady;
			//TimedOut = null;
			//DocumentsReady = null;
		}
	}
}