﻿using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;

namespace Otus.Teaching.Linq.ATM.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();
            var login = "snow";
            var password = "111";
            var id = 1;
            var cashAmount1 = 10600m;     // decimal
            var cashAmount2 = 3000000;  // int
            var incorrectLogin = "incorrect_login";
            var incorrectPassword = "incorrect_password";

            // 1
            var user = atmManager.FindUserByCredentials(login, password);
            System.Console.WriteLine($"\nUser with specified credentials:\n{user}");

            var userIncorrect = atmManager.FindUserByCredentials(incorrectLogin, incorrectPassword);
            System.Console.WriteLine($"\nUser with specified credentials:\n{userIncorrect}");

            // 2
            System.Console.WriteLine($"\nAll accounts of user {user.Login}:");

            var accounts = atmManager.ShowAllUserAccounts(user);
            if (!accounts.Any())
            {
                System.Console.WriteLine($"Accounts of user with login {user.Login} not found");
            }
            else
            {
                accounts.ToList().ForEach(ac => System.Console.WriteLine(ac));
            }

            // 3
            System.Console.WriteLine($"\nAll accounts with history of user {user.Login}:");

            var accountWithHistoty = atmManager.ShowAllUserAccountsIncludingEachAccountHistory(user);
            if (!accountWithHistoty.Any())
            {
                System.Console.WriteLine($"Accounts with history of user with login {user.Login} not found");
            }
            else
            {
                accountWithHistoty.ToList().ForEach(entry => System.Console.WriteLine($"\nAccount info:\n{entry.Account}\nOperations History:\n{entry.History}"));
            }

            //System.Console.WriteLine($"\nAll accounts of user {userIncorrect.Login}");
            //atmManager.ShowAllUserAccountsIncludingEachAccountHistory(userIncorrect);

            // 4
            System.Console.WriteLine($"\nAll income operations of user {user.Login}:");

            var incomeOperations = atmManager.ShowAllIncomeOperations(user);
            if (!incomeOperations.Any())
            {
                System.Console.WriteLine($"Income operations for user with login {user.Login} not found");
            }
            else
            {
                incomeOperations.ToList().ForEach(entry => System.Console.WriteLine($"\nOwner: {entry.Owner}\nOperations History:\n{entry.History}"));
            }

            // 5
            System.Console.WriteLine($"\nAll users with money amount more than {cashAmount1:G}:");

            var usersWithSomeAmount = atmManager.ShowAllUsersWithCashMoreThanSpecified(cashAmount1);
            if (!usersWithSomeAmount.Any())
            {
                System.Console.WriteLine($"Users with accounts with cash more than {cashAmount1} not found");
            }
            else
            {
                usersWithSomeAmount.ToList().ForEach(u => System.Console.WriteLine($"User info:\n{u.User}\nCash amount on account: {u.Cash}\n"));
            }

            var usersWithSomeAmount2 = atmManager.ShowAllUsersWithCashMoreThanSpecified(cashAmount2);
            if (!usersWithSomeAmount2.Any())
            {
                System.Console.WriteLine($"Users with accounts with cash more than {cashAmount2} not found");
            }
            else
            {
                usersWithSomeAmount2.ToList().ForEach(u => System.Console.WriteLine($"User info:\n{u.User}\nCash amount on account: {u.Cash}\n"));
            }

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
        }

        static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}