﻿using System;
using System.Collections.Generic;
using System.Linq;
using Otus.Teaching.Linq.ATM.Core.Entities;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        #region N1
        /// <summary>
        /// 1. Вывод информации о заданном аккаунте по логину и паролю;
        /// </summary>
        /// <param name="login"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User FindUserByCredentials(string login, string password) => Users.FirstOrDefault(u => u.Login == login && u.Password == password) ?? new NullUser(login);

        #endregion

        #region N2
        /// <summary>
        /// 2. Вывод данных о всех счетах заданного пользователя;
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<Account> ShowAllUserAccounts(User user) => Accounts.Where(ac => ac.UserId == user.Id);
        #endregion

        #region N3
        /// <summary>
        /// 3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<(Account Account, OperationsHistory History)> ShowAllUserAccountsIncludingEachAccountHistory(User user)
        {
            return Accounts.Where(ac => ac.UserId == user.Id).Join(History, ac => ac.Id, h => h.AccountId, (ac, h) => (Account: ac, History: h));
        }
        #endregion

        #region N4
        /// <summary>
        /// 4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта;
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public IEnumerable<(string Owner, OperationsHistory History)> ShowAllIncomeOperations(User user)
        {
            return History.Where(h => h.OperationType == OperationType.InputCash).Join(Accounts.Where(ac => ac.UserId == user.Id), h => h.AccountId, ac => ac.Id, (h, ac) => (Owner: user.Login, History: h));
        }
        #endregion

        #region N5
        /// <summary>
        /// 5. Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        public IEnumerable<(User User, decimal Cash)> ShowAllUsersWithCashMoreThanSpecified(decimal amount)
        {
            return Accounts.Where(ac => ac.CashAll > amount).Join(Users, ac => ac.UserId, u => u.Id, (ac, u) => (User: u, Cash: ac.CashAll));
        }
        #endregion
    }}