﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string MiddleName { get; set; }
        public string Phone { get; set; }
        public string PassportSeriesAndNumber { get; set; }
        public DateTime RegistrationDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nFirst Name: {FirstName}\nSurname: {SurName}\nMiddle Name: {MiddleName}\nPhone: {Phone}" +
                $"\nPassport Series And Number: {PassportSeriesAndNumber}\nRegistration Date: {RegistrationDate}\nLogin: {Login}";
        }

        public virtual bool IsNull
        {
            get { return false; }
        }
    }

    public sealed class NullUser : User
    {
        private string _login;
        private const string _message = "with login '{0}' ";

        public NullUser()
        {

        }

        public NullUser(string login)
        {
            _login = login;
        }

        public override bool IsNull
        {
            get { return true; }
        }

        public override string ToString()
        {
            return $"User {(_login == null ? "" : string.Format(_message, _login))}not found";
        }
    }
}