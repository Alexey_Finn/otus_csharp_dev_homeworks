﻿using RabbitMQ.Client;
using System;
using System.Text;

namespace Producer
{
	class Program
	{

		static void Main()
		{
			using (var connection = GetRabbitConnection())
			using (var channel = connection.CreateModel())
			{
				var counter = 0;
				do
				{
					string messageDirect = $"[Direct] Hello OTUS Students! Message #{counter+1} {DateTime.Now}";
					var bodyDirect = Encoding.UTF8.GetBytes(messageDirect);
					string messageTopic = $"[Topic] Hello OTUS Students! Message #{counter+1} {DateTime.Now}";
					var bodyTopic = Encoding.UTF8.GetBytes(messageTopic);

					var exchangeNameDirect = "exchangeName-Direct";
					var queueNameDirect = "queue_name_direct_111";
					var routingKey1 = "routing-key-1";
					var exchangeNameTopic = "exchangeName-Topic.#";
					var queueNameTopic = "queue_name_Topic_111";
					var routingKey2 = "routing-key-2";

					// Direct
					channel.ExchangeDeclare(exchangeNameDirect, ExchangeType.Direct);
					channel.QueueDeclare(queueNameDirect, false, false, false, null);
					channel.QueueBind(queueNameDirect, exchangeNameDirect, routingKey1, null);

					channel.BasicPublish(exchange: exchangeNameDirect,
										 routingKey: routingKey1,
										 basicProperties: null,
										 body: bodyDirect);
					Console.WriteLine($"Отправлено {messageDirect}");

					// Topic
					channel.ExchangeDeclare(exchangeNameTopic, ExchangeType.Direct);
					channel.QueueDeclare(queueNameTopic, false, false, false, null);
					channel.QueueBind(queueNameTopic, exchangeNameTopic, routingKey1, null);

					channel.BasicPublish(exchange: exchangeNameTopic,
										 routingKey: routingKey1,
										 basicProperties: null,
										 body: bodyTopic);
					Console.WriteLine($"Отправлено {messageTopic}");
					counter++;
				} while (counter < 10);
			}

			Console.WriteLine(" Press [enter] to exit.");
			Console.ReadLine();
		}

		static private IConnection GetRabbitConnection()
		{
			ConnectionFactory factory = new ConnectionFactory
			{
				UserName = "qfuqtywt",
				Password = "4-KC_Ze0o8oaOTBB3gtD7im2S2XJm2zk",
				VirtualHost = "qfuqtywt",
				HostName = "kangaroo.rmq.cloudamqp.com"
			};

			IConnection conn = factory.CreateConnection();
			return conn;
		}
	}
}
