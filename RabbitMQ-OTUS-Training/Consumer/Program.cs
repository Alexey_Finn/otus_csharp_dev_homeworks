﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Consumer
{
	class Program
	{
		static void Main()
		{
			ConsumeDirect();
			ConsumeTopic();
		}

		private static void ConsumeDirect()
		{
			Console.WriteLine($"Direct consumer");
			using (var connection = GetRabbitConnection())
			using (var channel = connection.CreateModel())
			{
				var consumer = new EventingBasicConsumer(channel);

				var exchangeNameDirect = "exchangeName-Direct";
				var queueNameDirect = "queue_name_direct_111";
				var routingKey = "routing-key";

				channel.ExchangeDeclare(exchangeNameDirect, ExchangeType.Direct);
				channel.QueueDeclare(queueNameDirect, false, false, false, null);
				channel.QueueBind(queueNameDirect, exchangeNameDirect, routingKey, null);

				consumer.Received += (model, ea) =>
				{
					var body = ea.Body;
					var message = Encoding.UTF8.GetString(body.ToArray());
					Console.WriteLine(" [x] Received {0}", message);
				};
				channel.BasicConsume(queue: queueNameDirect,
									 autoAck: true,
									 consumer: consumer);

				Console.WriteLine(" Press [enter] to exit.");
				Console.ReadLine();
			}
		}
		private static void ConsumeTopic()
		{
			Console.WriteLine($"Topic consumer");
			using (var connection = GetRabbitConnection())
			using (var channel = connection.CreateModel())
			{
				var consumer = new EventingBasicConsumer(channel);

				var exchangeNameTopic = "exchangeName-Topic.some.thing";
				var queueNameTopic = "queue_name_Topic_111";
				var routingKey2 = "routing-key-2";

				channel.ExchangeDeclare(exchangeNameTopic, ExchangeType.Direct);
				channel.QueueDeclare(queueNameTopic, false, false, false, null);
				channel.QueueBind(queueNameTopic, exchangeNameTopic, routingKey2, null);

				consumer.Received += (model, ea) =>
				{
					var body = ea.Body;
					var message = Encoding.UTF8.GetString(body.ToArray());
					Console.WriteLine(" [x] Received {0}", message);
				};
				channel.BasicConsume(queue: queueNameTopic,
									 autoAck: true,
									 consumer: consumer);

				Console.WriteLine(" Press [enter] to exit.");
				Console.ReadLine();
			}
		}

		static private IConnection GetRabbitConnection()
		{
			ConnectionFactory factory = new ConnectionFactory
			{
				UserName = "qfuqtywt",
				Password = "4-KC_Ze0o8oaOTBB3gtD7im2S2XJm2zk",
				VirtualHost = "qfuqtywt",
				HostName = "kangaroo.rmq.cloudamqp.com"
			};

			IConnection conn = factory.CreateConnection();
			return conn;
		}
	}
}