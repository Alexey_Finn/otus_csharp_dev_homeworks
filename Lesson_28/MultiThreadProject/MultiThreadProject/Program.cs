﻿using System;
using System.Diagnostics;

namespace MultiThreadProject
{
	class Program
	{
		static void Main(string[] args)
		{
			var first = new ArraySummator(100_000);
			DoMeasurements(first);

			var second = new ArraySummator(1_000_000);
			DoMeasurements(second);

			var third = new ArraySummator(10_000_000);
			DoMeasurements(third);

			var fourth = new ArraySummator(100_000_000);
			DoMeasurements(fourth);
		}

		private static void DoMeasurements(ArraySummator array)
		{
			Console.WriteLine($"\nNew measurements of array with {array.GetInnerArrayCount()} length");
			var sw = new Stopwatch();

			sw.Start();
			var serialSum = array.GetSerialSum();
			sw.Stop();
			var serialCounter = sw.Elapsed.TotalMilliseconds;

			sw.Restart();
			var parallelLinqSum = array.GetParallelLinqSum();
			sw.Stop();
			var parallelLinqCounter = sw.Elapsed.TotalMilliseconds;

			sw.Restart();
			var customListOfThreadSum_SkipTake = array.GetListOfThreadSum_SkipTake();
			sw.Stop();
			var customListOfThreadSum_SkipTakeCounter = sw.Elapsed.TotalMilliseconds;

			sw.Restart();
			var customListOfThreadSum_LinqGroupBy = array.GetListOfThreadSum_LinqGroupBy();
			sw.Stop();
			var customListOfThreadSum_LinqGroupByCounter = sw.Elapsed.TotalMilliseconds;

			Console.WriteLine(
				$"serialSum = {serialSum}" +
				$"\nparallelLinqSum = {parallelLinqSum}" +
				$"\ncustomListOfThreadSum_SkipTake = {customListOfThreadSum_SkipTake}" +
				$"\ncustomListOfThreadSum_LinqGroupBy = {customListOfThreadSum_LinqGroupBy}");
			WriteResultOfCompare(serialSum == customListOfThreadSum_SkipTake
				&& serialSum == parallelLinqSum && serialSum == customListOfThreadSum_LinqGroupBy);
			Console.WriteLine(
				"Measurement results (in ms):" +
				$"\n{serialCounter,10} - Serial sum" +
				$"\n{parallelLinqCounter,10} - Parallel Linq sum" +
				$"\n{customListOfThreadSum_SkipTakeCounter,10} - Custom List Of Threads sum (Skip-Take)" +
				$"\n{customListOfThreadSum_LinqGroupByCounter,10} - Custom List Of Threads sum (LINQ Group By)");
		}

		private static void WriteResultOfCompare(bool result)
		{
			Console.Write("Result of compare equality: ");
			if (result)
			{
				Console.ForegroundColor = ConsoleColor.Green;
			}
			else
			{
				Console.ForegroundColor = ConsoleColor.Red;
			}
			Console.Write(result);
			Console.ResetColor();
			Console.WriteLine();
		}
	}
}