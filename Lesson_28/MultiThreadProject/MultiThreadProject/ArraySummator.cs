﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace MultiThreadProject
{
	public class ArraySummator
	{
		private static readonly object _lock = new();
		private static int[] _array;

		public ArraySummator(int count)
		{
			if (count < 10_000)
			{
				count = 10_000;
			}

			_array = new int[count];
			FillArray();
		}

		public int[] GetInnerArray() => _array;
		public int GetInnerArrayCount() => _array.Length;

		public int GetSerialSum()
		{
			var result = 0;

			foreach (var item in _array)
			{
				result += item;
			}

			return result;
		}

		public int GetListOfThreadSum_LinqGroupBy()
		{
			var countOfThreads = Environment.ProcessorCount;
			var threadPool = new List<Thread>(countOfThreads + 1);
			var divider = _array.Length / countOfThreads;
			var result = 0;

			var chunks = _array
					.Select((s, i) => new { Value = s, Index = i })
					.GroupBy(x => x.Index / divider)
					.Select(grp => grp.Select(x => x.Value));

			foreach (var chunk in chunks)
			{
				var thread = new Thread(() => AddResultToSum(chunk, ref result));
				thread.Start();
				threadPool.Add(thread);
			}

			threadPool.ForEach(thread => thread.Join());

			return result;
		}
		public int GetListOfThreadSum_SkipTake()
		{
			var countOfThreads = Environment.ProcessorCount;
			var threadPool = new List<Thread>(countOfThreads + 1);
			var divider = _array.Length / countOfThreads;
			var result = 0;

			for (int i = 0; i <= countOfThreads; i++)
			{
				var chunk = _array.Skip(i * divider).Take(divider);
				var thread = new Thread(() => AddResultToSum(chunk, ref result));
				thread.Start();
				threadPool.Add(thread);
			}

			threadPool.ForEach(thread => thread.Join());

			return result;
		}

		public int GetParallelLinqSum() => _array.AsParallel().Sum();

		private static void FillArray()
		{
			var rnd = new Random();

			for (int i = 0; i < _array.Length; i++)
			{
				_array[i] = rnd.Next(1, 20);
			}
		}

		private void AddResultToSum(IEnumerable<int> chunk, ref int sum)
		{
			var result = 0;

			foreach (var item in chunk)
			{
				result += item;
			}

			lock (_lock)
			{
				sum += result;
			}
		}
	}
}