﻿using System;

namespace Interfaces.Interfaces
{
    public interface ITransaction
    {
        DateTimeOffset Date { get; }
        ICurrencyAmount Amount { get; }
    }
}