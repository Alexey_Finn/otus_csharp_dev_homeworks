﻿namespace Interfaces.Interfaces
{
    public interface ITransactionParser
    {
        ITransaction Parse(string input);
    }
}