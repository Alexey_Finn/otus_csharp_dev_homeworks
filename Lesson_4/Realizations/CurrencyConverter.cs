﻿using Interfaces.Interfaces;

namespace Interfaces.Realizations
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            // 1 to 1
            return new CurrencyAmount(amount, currencyCode);
        }
    }
}
