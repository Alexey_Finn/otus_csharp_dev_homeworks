﻿using Interfaces.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System.Net.Http;

namespace Interfaces.Realizations.Converters
{
    public class ConverterToYen : ExchangeRatesApiConverter
    {
        private const string DollarCurrency = "JPY";

        public ConverterToYen(HttpClient httpClient, IMemoryCache memoryCache) : base(httpClient, memoryCache)
        {
        }

        public ICurrencyAmount Convert(ICurrencyAmount amount)
        {
            return ConvertCurrency(amount, DollarCurrency);
        }
    }
}