﻿using Interfaces.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System.Net.Http;

namespace Interfaces.Realizations.Converters
{
    public class ConverterToEuro : ExchangeRatesApiConverter
    {
        private const string EuroCurrency = "EUR";

        public ConverterToEuro(HttpClient httpClient, IMemoryCache memoryCache) : base(httpClient, memoryCache)
        {
        }

        public ICurrencyAmount Convert(ICurrencyAmount amount)
        {
            return ConvertCurrency(amount, EuroCurrency);
        }
    }
}