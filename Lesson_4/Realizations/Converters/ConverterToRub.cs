﻿using Interfaces.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System.Net.Http;

namespace Interfaces.Realizations.Converters
{
    public class ConverterToRuble : ExchangeRatesApiConverter
    {
        private const string DollarCurrency = "RUB";

        public ConverterToRuble(HttpClient httpClient, IMemoryCache memoryCache) : base(httpClient, memoryCache)
        {
        }

        public ICurrencyAmount Convert(ICurrencyAmount amount)
        {
            return ConvertCurrency(amount, DollarCurrency);
        }
    }
}