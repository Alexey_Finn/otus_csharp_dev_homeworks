﻿using Interfaces.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using System.Net.Http;

namespace Interfaces.Realizations.Converters
{
    public class ConverterToDollar : ExchangeRatesApiConverter
    {
        private const string DollarCurrency = "USD";

        public ConverterToDollar(HttpClient httpClient, IMemoryCache memoryCache) : base(httpClient, memoryCache)
        {
        }

        public ICurrencyAmount Convert(ICurrencyAmount amount)
        {
            return ConvertCurrency(amount, DollarCurrency);
        }
    }
}