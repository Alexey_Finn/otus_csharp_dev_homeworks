﻿using Interfaces.Interfaces;
using System;
using System.Collections.Generic;

namespace Interfaces.Realizations
{
    public class CurrencyAmount : ICurrencyAmount, IEquatable<ICurrencyAmount>
    {
        private readonly decimal _amount;
        private readonly string _currencyCode;

        public CurrencyAmount(string amount, string currencyCode)
        {
            // без проверок для простоты
            _amount = decimal.Parse(amount);
            _currencyCode = currencyCode;
        }

        public CurrencyAmount(ICurrencyAmount currencyAmount)
        {
            _amount = currencyAmount.Amount;
            _currencyCode = currencyAmount.CurrencyCode;
        }

        public CurrencyAmount(ICurrencyAmount currencyAmount, string currencyCode) 
            : this(currencyAmount)
        {
            _currencyCode = currencyCode;
        }

        public string CurrencyCode => _currencyCode;

        public decimal Amount => _amount;

        public override bool Equals(object obj)
        {
            return Equals(obj as ICurrencyAmount);
        }

        public bool Equals(ICurrencyAmount other)
        {
            return other != null &&
                   CurrencyCode == other.CurrencyCode &&
                   Amount == other.Amount;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(CurrencyCode, Amount);
        }

        public static bool operator ==(ICurrencyAmount left, CurrencyAmount right)
        {
            return EqualityComparer<ICurrencyAmount>.Default.Equals(left, right);
        }

        public static bool operator !=(ICurrencyAmount left, CurrencyAmount right)
        {
            return !(left == right);
        }

        public static bool operator ==(CurrencyAmount left, ICurrencyAmount right)
        {
            return EqualityComparer<ICurrencyAmount>.Default.Equals(left, right);
        }

        public static bool operator !=(CurrencyAmount left, ICurrencyAmount right)
        {
            return !(left == right);
        }

        public override string ToString()
        {
            return $"{Amount} {CurrencyCode}";
        }
    }
}
