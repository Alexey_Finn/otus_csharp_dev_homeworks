﻿using Interfaces.Interfaces;
using System;
using System.Collections.Generic;

namespace Interfaces.Realizations
{
    public class InMemoryTransactionRepository : ITransactionRepository
    {
        private readonly List<ITransaction> transactions;

        public InMemoryTransactionRepository(List<ITransaction> transactions)
        {
            this.transactions = transactions;
        }

        public void AddTransaction(ITransaction transaction)
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            return obj is InMemoryTransactionRepository repository &&
                   EqualityComparer<List<ITransaction>>.Default.Equals(transactions, repository.transactions);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(transactions);
        }

        public ITransaction[] GetTransactions()
        {
            return transactions.ToArray();
        }
    }
}
