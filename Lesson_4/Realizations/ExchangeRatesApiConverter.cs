﻿using Interfaces.Interfaces;
using Interfaces.Models;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Interfaces.Realizations
{
    public class ExchangeRatesApiConverter : ICurrencyConverter
    {
        private CurrencyConverter converter = new CurrencyConverter();
        private readonly HttpClient httpClient;
        private readonly IMemoryCache memoryCache;
        private const string apiKey = "50648f8bc11e8b524d6d3bdfb53d7e9a";
        private const string endpointAddress = @"http://api.exchangeratesapi.io/v1/latest?access_key={0}";

        public ExchangeRatesApiConverter(HttpClient httpClient, IMemoryCache memoryCache)
        {
            this.httpClient = httpClient;
            this.memoryCache = memoryCache;
        }

        public ICurrencyAmount ConvertCurrency(ICurrencyAmount amount, string currencyCode)
        {
            // перевожу сперва в евро для валидного ответа из апихи. но тут при переводе нужны данные, которые сейчас 1 к 1
             return GetAsyncAmount(converter.ConvertCurrency(amount, "EUR"), currencyCode).GetAwaiter().GetResult();

            // return GetAsyncAmount(amount, currencyCode).GetAwaiter().GetResult();
        }

        private async Task<ICurrencyAmount> GetAsyncAmount(ICurrencyAmount amount, string currencyCode)
        {
            var cache = await memoryCache.GetOrCreateAsync("cached", entry =>
            {
                // для примера тут 5 секунд, но можно и 1 день выставить из-за ограничения бесплатной учетки
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromSeconds(5);
                var result = GetCurrencyResponse();
                memoryCache.Set("cached", result);
                Console.WriteLine("-------> Result from API");

                return result;
            });


            if (cache.Rates.TryGetValue(currencyCode, out double value))
            {
                var deserialized = cache.Rates.Single(r => r.Key.ToLowerInvariant() == currencyCode.ToLowerInvariant());
                return new CurrencyAmount((Convert.ToDecimal(value) * amount.Amount).ToString(), deserialized.Key);
            }
            else
            {
                throw new Exception($"Cannot get rates from cache or API for next parameters:\n" +
                    $"amount.Amount: {amount.Amount},\namount.CurrencyCode: {amount.CurrencyCode},\ncurrencyCode: {currencyCode}");
            }
        }

        private async Task<CurrencyResponse> GetCurrencyResponse()
        {
            var response = await httpClient.GetAsync(string.Format(endpointAddress, apiKey));

            response.EnsureSuccessStatusCode();

            var responseBody = await response.Content.ReadAsStringAsync();
            var responseText = responseBody;
            var currencyResponse = CurrencyResponse.FromJson(responseText);

            return currencyResponse;
        }
    }
}
