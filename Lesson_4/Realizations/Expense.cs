﻿using Interfaces.Interfaces;
using System;
using System.Collections.Generic;

namespace Interfaces.Realizations
{
    public class Expense : ITransaction, IEquatable<Expense>
    {
        private readonly DateTimeOffset _date;
        private readonly ICurrencyAmount _amount;

        public Expense(DateTimeOffset date, ICurrencyAmount amount, string category, string destination)
        {
            _amount = amount;
            _date = date;
            Category = category;
            Destination = destination;
        }

        public DateTimeOffset Date => _date;

        public ICurrencyAmount Amount => _amount;

        public string Category { get; }

        public string Destination { get; }

        public override bool Equals(object obj)
        {
            return Equals(obj as Expense);
        }

        public bool Equals(Expense other)
        {
            return !(other is null) &&
                   Date.Equals(other.Date) &&
                   EqualityComparer<ICurrencyAmount>.Default.Equals(Amount, other.Amount) &&
                   Category == other.Category &&
                   Destination == other.Destination;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Date, Amount, Category, Destination);
        }

        public override string ToString()
        {
            return $"Expense {Amount} from {Date} to {Destination} with category {Category} ";
        }

        public static bool operator ==(ITransaction left, Expense right)
        {
            return EqualityComparer<ITransaction>.Default.Equals(left, right);
        }

        public static bool operator !=(ITransaction left, Expense right)
        {
            return !(left == right);
        }

        public static bool operator ==(Expense left, ITransaction right)
        {
            return EqualityComparer<ITransaction>.Default.Equals(left, right);
        }

        public static bool operator !=(Expense left, ITransaction right)
        {
            return !(left == right);
        }
    }
}
