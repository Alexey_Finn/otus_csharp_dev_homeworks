﻿using Interfaces.Interfaces;
using System;
using System.Linq;

namespace Interfaces.Realizations
{
    public class BudgetApplication : IBudgetApplication
    {
        private readonly ITransactionRepository transactionRepository;
        private readonly ITransactionParser transactionParser;
        private readonly ICurrencyConverter currencyConverter;

        public BudgetApplication(ITransactionRepository transactionRepository, ICurrencyConverter currencyConverter, ITransactionParser transactionParser)
        {
            this.transactionRepository = transactionRepository;
            this.transactionParser = transactionParser;
            this.currencyConverter = currencyConverter;

        }

        public void AddTransaction(string input)
        {
            var transaction = transactionParser.Parse(input);

            transactionRepository.AddTransaction(transaction);
        }

        public void OutputTransactions()
        {
            foreach (var transaction in transactionRepository.GetTransactions())
            {
                Console.WriteLine($"Date: {transaction.Date}, Amount: {transaction.Amount}");
            }
        }

        public void OutputBalanceInCurrency(string currencyCode)
        {
            var transactions = transactionRepository.GetTransactions();
            var transactionsInCurrentCurrencyCode = transactions.Select(t => currencyConverter.ConvertCurrency(t.Amount, currencyCode));
            var sum = transactionsInCurrentCurrencyCode.Sum(t => t.Amount);

            Console.WriteLine($"Current balance: {sum} {currencyCode}");
        }
    }
}