﻿using Interfaces.Interfaces;
using System;

namespace Interfaces.Realizations
{
    public class TransactionParser : ITransactionParser
    {
        public ITransaction Parse(string input)
        {
            var array = input.Split(" ");
            var amount = array[1];
            var currencyCode = array[2];
            var category = array[3];
            var destination = array[4];
            var currencyAmount = new CurrencyAmount(amount, currencyCode);

            return new Expense(DateTime.Today, currencyAmount, category, destination);
        }
    }
}
