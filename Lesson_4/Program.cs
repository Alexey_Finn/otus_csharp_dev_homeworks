﻿using Interfaces.Realizations;
using Interfaces.Realizations.Converters;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Net.Http;
using System.Threading;

namespace Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            var httpClient = new HttpClient();
            var optionsAccessor = new MemoryCacheOptions();
            var memoryCache = new MemoryCache(optionsAccessor);

            var dollarConverter = new ConverterToDollar(httpClient, memoryCache);
            var rubleConverter = new ConverterToRuble(httpClient, memoryCache);
            var yenConverter = new ConverterToYen(httpClient, memoryCache);

            var amount = new CurrencyAmount("100", "EUR");
            var result = default(CurrencyAmount);

            Console.WriteLine($"Input parameter: {amount}\n");

            for (var i = 0; i < 10; i++)
            {
                result = (CurrencyAmount)dollarConverter.Convert(amount);

                Console.WriteLine(result);

                result = (CurrencyAmount)rubleConverter.Convert(amount);

                Console.WriteLine(result);

                result = (CurrencyAmount)yenConverter.Convert(amount);

                Console.WriteLine(result);

                Console.WriteLine($"-------\nSleep for 1 sec.\n");
                Thread.Sleep(TimeSpan.FromSeconds(1));
            }

            Console.WriteLine($"Done.");
        }       
    }
}