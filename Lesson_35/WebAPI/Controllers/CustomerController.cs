﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace WebAPI.Controllers
{
	[ApiController]
	[Route("users")]
	public class CustomerController : Controller
	{
		private CustomerRepository repository;

		public CustomerController(CustomerRepository repository)
		{
			this.repository = repository;
		}

		[HttpGet]
		public IEnumerable<Customer> Get()
		{
			return repository.GetAll();
		}

		[HttpGet("{id}")]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		public async Task<IActionResult> GetByIdAsync(int id)
		{
			var customer = await repository.GetByIdAsync(id);
			if (customer is null) return NotFound(id);
			return Ok(customer);
		}

		[HttpPost]
		[ProducesResponseType(StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status409Conflict)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		public ActionResult Add([FromBody] Customer customer)
		{
			var result = repository.Add(customer);

			if (result == 1)
			{
				return Ok(customer);
			}
			else if (result == -19)
			{
				return Conflict(new { result, customer});
			}
			else
				return BadRequest(new { result, customer });
		}
	}
}
