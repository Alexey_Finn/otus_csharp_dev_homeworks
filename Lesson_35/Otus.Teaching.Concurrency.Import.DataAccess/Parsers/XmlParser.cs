﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
	public class XmlParser
		: IDataParser<List<Customer>>
	{
		private readonly string resourceFilePath;
		XmlSerializer parser;
		List<Customer> result;

		public XmlParser(string inputFile)
		{
			resourceFilePath = inputFile;
			result = new List<Customer>();
			parser = new XmlSerializer(typeof(CustomersList));
		}

		public List<Customer> Parse()
		{
			using (FileStream fs = new FileStream(resourceFilePath, FileMode.Open))
			{
				var customersList = (CustomersList)parser.Deserialize(fs);

				result = customersList.Customers.Select(c => new Customer
				{
					Id = c.Id,
					Email = c.Email,
					FullName = c.FullName,
					Phone = c.Phone
				}).ToList();
			}

			return result;
		}
	}
}