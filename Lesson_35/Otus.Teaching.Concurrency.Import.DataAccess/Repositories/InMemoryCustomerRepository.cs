﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Core.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
	public class InMemoryCustomerRepository : DbContext, IRepository<Customer>
	{
        public DbSet<Customer> Customers { get; set; }
        public InMemoryCustomerRepository(DbContextOptions options)
            : base(options)
        {
            Database.EnsureCreated();   // создаем базу данных при первом обращении
        }

		public void Add(Customer t)
		{
			Customers.Add(t);
		}

		public Task AddAsync(Customer t)
		{
			var result = Customers.AddAsync(t);
			return result.AsTask();
		}

		public void Update(Customer t)
		{
			var resultForUpdate = Customers.FirstOrDefault(el => el.Id == t.Id);
			if (resultForUpdate is null) return;
			resultForUpdate.FullName = t.FullName;
			resultForUpdate.Phone = t.Phone;
			resultForUpdate.Email = t.Email;
		}

		public IEnumerable<Customer> GetAll()
		{
			return Customers;
		}

		public Customer GetById(int id)
		{
			return Customers.FirstOrDefault(el => el.Id == id);
		}

		public async Task<Customer> GetByIdAsync(int id)
		{
			return await Customers.FirstOrDefaultAsync(el => el.Id == id);
		}

		public void Delete(Customer t)
		{
			Customers.Remove(t);
		}

		public void DeleteById(int id)
		{
			var resultById = GetById(id);
			if (resultById is null) return;
			Customers.Remove(resultById);
		}
	}
}
