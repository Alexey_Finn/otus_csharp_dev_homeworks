using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
	public class CustomerRepository
		: DbContext, ICustomerRepository
	{
		static string connectionString = "Data Source=" + DbFile;
		private readonly static object _lock = new object();
		private static string DbFile
		{
			get { return @"C:\study\db\customersdata.db"; }
		}

		public CustomerRepository()
		{
			lock (_lock)
			{
				if (!File.Exists(DbFile))
				{
					Directory.CreateDirectory(@"C:\study\db");
					CreateDatebase();
				}
			}
		}

		public CustomerRepository(DbContextOptions options) : base(options)
		{
			lock (_lock)
			{
				if (!File.Exists(DbFile))
				{
					Directory.CreateDirectory(@"C:\study\db");
					CreateDatebase();
				}
			}
		}

		private static SqliteConnection MakeConnection()
		{
			return new SqliteConnection(connectionString);
		}

		public int AddCustomer(Customer customer)
		{
			//Add customer to data source

			Console.WriteLine($"==> Execution of \"AddCutomer\" has begun at thread {Thread.CurrentThread.ManagedThreadId}");
			using var connection = MakeConnection();

			int result;
			try
			{
				connection.Open();
				result = connection.Execute(
@"INSERT INTO Customer
    ( Id, FullName, Email, Phone ) VALUES
    ( @Id, @FullName, @Email, @Phone );", customer);
			}
			catch (SqliteException e)
			{
				Console.WriteLine(e.Message);

				result = e.SqliteErrorCode * (-1);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				result = 0;
			}

			Console.WriteLine($"Inserted record to db");
			return result;
		}

		public void ReadDatabase()
		{
			lock (_lock)
			{
				if (!File.Exists(DbFile)) return;
			}

			List<Customer> results;
			results = GetAllRecords();

			Console.WriteLine("Database reading result:");
			foreach (var item in results)
			{
				Console.WriteLine(item);
			}
			Console.WriteLine($"Total {results.Count} records was readed.");
		}

		private static List<Customer> GetAllRecords()
		{
			List<Customer> results;
			using (var cnn = MakeConnection())
			{
				cnn.Open();
				results = cnn.Query<Customer>(@"SELECT Id, FullName, Phone, Email FROM Customer ORDER BY Id").AsList();
			}

			return results;
		}

		private static void CreateDatebase()
		{
			using (var connection = MakeConnection())
			{
				connection.Open();
				connection.Execute(
@"create table Customer
(
    Id         INTEGER IDENTITY PRIMARY KEY,
    FullName   varchar(100) not null,
    Email      varchar(100) not null,
    Phone      varchar(100) not null
)");
			}
		}

		public int Add(Customer t)
		{
			return AddCustomer(t);
		}

		public void Delete(int id)
		{
			//Delete customer from data source

			Console.WriteLine($"==> Execution of \"Delete\" has begun at thread {Thread.CurrentThread.ManagedThreadId}");

			using var connection = MakeConnection();
			try
			{
				connection.Open();
				connection.Execute("DELETE FROM Customer WHERE Id = @id", new { id });
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}

			Console.WriteLine($"Deleted record from db");
		}

		public void Update(Customer t)
		{
			//Update customer from data source

			Console.WriteLine($"==> Execution of \"Update\" has begun at thread {Thread.CurrentThread.ManagedThreadId}");

			using var connection = MakeConnection();
			try
			{
				connection.Open();
				connection.Execute("UPDATE Customer SET FullName = @FullName, " +
					"Email = @Email, Phone = @Phone WHERE Id = @Id", t);
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}

			Console.WriteLine($"Updated record in db");
		}

		public IEnumerable<Customer> GetAll()
		{
			return GetAllRecords();
		}

		public Customer GetById(int id)
		{
			//GetById customer from data source

			Console.WriteLine($"==> Execution of \"GetById\" has begun at thread {Thread.CurrentThread.ManagedThreadId}");

			Customer result;
			using var connection = MakeConnection();
			try
			{
				connection.Open();
				result = connection.QueryFirstOrDefault<Customer>("SELECT * FROM Customer WHERE Id = @id", 
					new { id });
			}
			catch (Exception e)
			{
				Console.WriteLine(e.Message);
				throw;
			}

			Console.WriteLine($"Customer was returned from db");

			return result;
		}

		public Task<Customer> GetByIdAsync(int id)
		{
			return Task.FromResult(GetById(id));
		}

		void ICustomerRepository.AddCustomer(Customer customer)
		{
			Add(customer);
		}
	}
}