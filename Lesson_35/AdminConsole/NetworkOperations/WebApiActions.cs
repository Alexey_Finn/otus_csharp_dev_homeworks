﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace AdminConsole.NetworkOperations
{
	public class WebApiActions
	{
		public async Task Post(object content)
		{
			using var client = GetClient();
			var result = await client.PostAsJsonAsync("/users", content);
			Console.WriteLine($"Post method result: {result.StatusCode}");
		}

		public async Task Get(object content)
		{
			using var client = GetClient();
			var result = await client.GetFromJsonAsync($"/users/{content}", typeof(Customer));
			Console.WriteLine($"Get method result: {result}");
		}

		private static HttpClient GetClient() => new()
		{
			BaseAddress = new Uri("http://localhost:5000")
		};
	}
}