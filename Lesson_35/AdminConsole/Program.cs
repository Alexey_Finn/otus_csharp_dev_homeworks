﻿using AdminConsole.CustomersOperations;
using AdminConsole.MenuOperations;
using System;

namespace AdminConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			var menu = new Menu();
			Presentation(menu);
		}

		private static void Presentation(Menu menu)
		{
			bool doMenu = true;
			do
			{
				menu.DrawMenu();
				switch (menu.ChooseMenuOption())
				{
					case "1":
						Console.WriteLine("Запущен метод \"добавление случайного кастомера в бд\"");
						Actions.AddRandomCustomer();
						Console.WriteLine("Для продолжения нажмите любую кнопку.");
						Console.ReadKey();
						break;
					case "2":
						Console.WriteLine("Запущен метод \"добавление конкретного кастомера в бд\"");
						Actions.AddConcreteCustomer();
						Console.WriteLine("Для продолжения нажмите любую кнопку.");
						Console.ReadKey();
						break;
					case "3":
						Console.WriteLine("Запущен метод \"поиск кастомера по id\"");
						Actions.GetCustomerById();
						Console.WriteLine("Для продолжения нажмите любую кнопку.");
						Console.ReadKey();
						break;
					case "4":
						doMenu = false;
						Console.WriteLine("Выход из приложения. Нажмите любую клавишу для продолжения.");
						Console.ReadKey();
						break;
					default:
						menu.DrawValidationError();
						break;
				}
			} while (doMenu);
		}
	}
}
