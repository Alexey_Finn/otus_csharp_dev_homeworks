﻿using System;

namespace AdminConsole.MenuOperations
{
	internal class Menu
	{
		public void DrawMenu()
		{
			Console.Clear();
			Console.WriteLine($"-------Приложение по работе с кастомерами---------");
			Console.WriteLine("1. Добавить случайного кастомера в БД");
			Console.WriteLine("2. Добавить конкретного кастомер в БД");
			Console.WriteLine("3. Получить кастомера из БД");
			Console.WriteLine("4. Выход");
			Console.WriteLine($"--------------------------------------------------");
			Console.Write("Выберите пункт меню: ");
		}
		public string ChooseMenuOption()
		{
			return Console.ReadLine();
		}

		public void DrawValidationError()
		{
			Console.WriteLine("Некорректный ввод");
			Console.WriteLine("Нажмите любую клавишу, чтобы повторить ввод");
			Console.ReadKey();
		}
	}
}