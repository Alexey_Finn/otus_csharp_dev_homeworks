﻿using AdminConsole.CustomersOperations;
using AdminConsole.NetworkOperations;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;

namespace AdminConsole.MenuOperations
{
	public static class Actions
	{
		static CustomerGenerator customerGenerator = new();
		static WebApiActions webApi = new();

		public static void AddRandomCustomer()
		{
			var customer = customerGenerator.GetRandomCustomer();
			if (webApi.Post(customer).Wait(5000))
				Console.WriteLine("[Post] Success");
			else
				Console.WriteLine("[Post] Timeout");
		}

		public static void AddConcreteCustomer()
		{
			var customer = new Customer();
			int id = 0;
			string fullName = default, phone = default, email = default;

			while (true)
			{
				Console.Write("Введите id кастомера: ");
				if (!int.TryParse(Console.ReadLine(), out id))
					Console.WriteLine("Некорректный ввод id. Требуется число. Попробуйте еще раз");
				else
					break;
			}
			while (true)
			{
				Console.Write("Введите FullName кастомера: ");
				fullName = Console.ReadLine();

				if (string.IsNullOrWhiteSpace(fullName))
					Console.WriteLine("Некорректный ввод FullName. Требуется не пустая строка.");
				else
					break;
			}
			while (true)
			{
				Console.Write("Введите Phone кастомера: ");
				phone = Console.ReadLine();

				if (string.IsNullOrWhiteSpace(phone))
					Console.WriteLine("Некорректный ввод Phone. Требуется не пустая строка.");
				else
					break;
			}
			while (true)
			{
				Console.Write("Введите Email кастомера: ");
				email = Console.ReadLine();

				if (string.IsNullOrWhiteSpace(email))
					Console.WriteLine("Некорректный ввод Email. Требуется не пустая строка.");
				else
					break;
			}

			customer.Id = id;
			customer.FullName = fullName;
			customer.Email = email;
			customer.Phone = phone;

			if (webApi.Post(customer).Wait(5000))
				Console.WriteLine("[Post] Success");
			else
				Console.WriteLine("[Post] Timeout");
		}

		public static void GetCustomerById()
		{
			Console.Write($"Введите id для поиска кастомера: ");
			if (int.TryParse(Console.ReadLine(), out var result))
				if (webApi.Get(result).Wait(5000))
					Console.WriteLine("[Post] Success");
				else
					Console.WriteLine("[Post] Timeout");
			else
				Console.WriteLine($"Некорректный ввод id. Требуется число.");
		}
	}
}