﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Linq;

namespace AdminConsole.CustomersOperations
{
	public class CustomerGenerator
	{
		public Customer GetRandomCustomer()
		{
			const int count = 100;
			return RandomCustomerGenerator.Generate(count).Skip(new Random().Next(count)).First();
		}

		public Customer GetConcreteCustomer(int id, string fullName, string email, string phone)
		{
			return new Customer { Id = id, FullName = fullName, Email = email, Phone = phone };
		}
	}
}
