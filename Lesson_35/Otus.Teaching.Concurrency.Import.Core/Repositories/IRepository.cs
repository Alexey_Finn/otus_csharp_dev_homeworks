﻿using System.Collections.Generic;

namespace Otus.Teaching.Concurrency.Import.Core.Repositories
{
	public interface IRepository<T>
	{
		void Add(T t);
		void Delete(T t);
		void DeleteById(int id);
		void Update(T t);
		IEnumerable<T> GetAll();
		T GetById(int id);
	}
}