﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Teaching.Concurrency.Import.Loader
{
	public class Program
	{
		private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.xml");

		static void Main(string[] args)
		{
			var timeStamp = DateTime.Now;
			Console.WriteLine($"Loader started at {timeStamp}");
			var countOfRecords = 1000;

			if (args.Length != 0)
			{
				var indexOfCount = args.ToList().LastIndexOf("/count");
				var indexOfFilePath = args.ToList().LastIndexOf("/path");

				if (indexOfCount != -1 && int.TryParse(args[indexOfCount + 1].Replace("-", ""), out var result))
				{
					countOfRecords = result;
				}
				if (indexOfFilePath != -1 && args.Length >= indexOfFilePath + 2)
				{
					// Unhandled exception. System.UnauthorizedAccessException: Access to the path '' is denied.
					// process.ExitCode = -532462766
					//_dataFilePath = args[indexOfFilePath + 1].Replace("\"", "");
				}
			}


			Console.WriteLine($"Loader started with process Id={Process.GetCurrentProcess().Id}...");

			GenerateCustomersDataFile(countOfRecords);

			var loader = new FakeDataLoader();

			loader.LoadData();

			Console.WriteLine($"Loader finished at {DateTime.Now} after {(DateTime.Now - timeStamp).TotalSeconds} seconds");
		}

		public static void GenerateCustomersDataFile(int numberOfRecords = 1000)
		{
			var timeStamp = DateTime.Now;
			var xmlGenerator = new XmlGenerator(_dataFilePath, numberOfRecords);
			xmlGenerator.Generate();

			Console.WriteLine($"GenerateCustomersDataFile finished after {(DateTime.Now - timeStamp).TotalSeconds} seconds");
		}
	}
}