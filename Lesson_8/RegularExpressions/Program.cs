﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Drawing.Imaging;
using System.Net;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Linq;

namespace RegularExpressions
{
    class Program
    {
        static void Main(string[] args)
        {
            const string Pattern = @"(<img\s)+.*(src=\W*)+([^""|^\\]+)";
            const string FolderToSaveImages = "Photos";

            Console.Write("Enter URL: ");
            var url = Console.ReadLine();
            Console.WriteLine($"You are entered:\n{url}");
            Utils.DrawDivider();

            if (!url.StartsWith("https://") && !url.StartsWith("http://"))
            {
                url = string.Concat("https://", url);
            }

            var downloader = new Downloader();
            var data = downloader.Download(url);

            var matches = Regex.Matches(data, Pattern);
            var listOfUrls = new List<string>();

            Console.WriteLine($"Found {matches.Count} matches!");

            Console.WriteLine($"There are {(matches.Count > 0 ? "founded matches" : "nothing to show")}\n");
            foreach (Match item in matches)
            {
                listOfUrls.Add(item.Groups[3].Value);
                Console.WriteLine(item.Groups[3].Value);
            }
            Utils.DrawDivider();

            if (!listOfUrls.Any())
            {
                return;
            }

            Console.WriteLine($"Trying to save founded images.");
            Console.Write($"Need to erase destination folder before saving images? y/N ");

            if (Console.ReadKey() == new ConsoleKeyInfo('y',ConsoleKey.Y, false, false, false))
            {
                Utils.CleanupDirectory(FolderToSaveImages);
                Console.WriteLine($"\nDirectory '{FolderToSaveImages}' was cleaned up.");
            }
            Utils.DrawDivider(carriageReturnBeforeDivider: true);

            foreach (var imageUrl in listOfUrls)
            {
                downloader.DownloadImage(imageUrl, FolderToSaveImages);
            }

            Utils.OpenFolder(FolderToSaveImages);
        }

        class Downloader
        {
            public string Download(string url)
            {
                var result = default(string);
                var request = WebRequest.Create(url);
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }

                return result;
            }

            public IEnumerable<string> DownloadAsLines(string url)
            {
                var result = new List<string>();
                var request = WebRequest.Create(url);
                using (var response = (HttpWebResponse)request.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    while (!reader.EndOfStream)
                    {
                        result.Add(reader.ReadLine());
                    }
                }

                return result;
            }

            public void DownloadImage(string url, string pathToSave)
            {
                // метод и класс не должны этим заниматься, но для упрощения решил сделать тут
                if (url.StartsWith("//"))
                {
                    url = url.Remove(0,2);
                }
                if (!url.StartsWith("https://") && !url.StartsWith("http://"))
                {
                    url = string.Concat("https://", url);
                }

                try
                {
                    using (WebClient webClient = new WebClient())
                    {
                        var data = webClient.DownloadData(url);

                        using (var ms = new MemoryStream(data))
                        using (var image = Image.FromStream(ms))
                        {
                            var fileName = $"image_{DateTime.UtcNow:yyyy_MM_dd_hh_mm_ss}_{DateTime.UtcNow.Ticks}.jpg";
                            var imageFormat = ImageFormat.Jpeg;

                            Utils.CreateDirectoryIfNotExist(pathToSave);
                            
                            image.Save(Path.Combine(pathToSave, fileName), imageFormat);
                            Console.WriteLine($"Image was saved -> '{fileName}'");
                        }
                    }
                }
                catch (Exception e)
                {
                    Utils.DrawDivider();
                    Console.WriteLine($"An exception has occured during saving file." +
                        $"\nUrl: {url}\nException: {e.Message}");
                    Utils.DrawDivider();
                }
            }
        }

        static class Utils
        {
            internal static void DrawDivider(bool carriageReturnBeforeDivider = false) => Console.WriteLine($"{(carriageReturnBeforeDivider ? Environment.NewLine : "")}" + "----------------------------------");
            internal static string GetRegularExpression() => "";

            /// <summary>
            /// Warning! Erasing all files in directory specified. Use on your own risk!
            /// </summary>
            /// <param name="path"></param>
            internal static void CleanupDirectory(string path) => Directory.EnumerateFiles(path).ToList().ForEach(f => File.Delete(f));
            internal static void CreateDirectoryIfNotExist(string path)
            {
                if(!Directory.Exists(path)) 
                {
                    Directory.CreateDirectory(path);
                } 
            }
            internal static void OpenFolder(string path) => System.Diagnostics.Process.Start("explorer.exe", path);
        }
    }
}